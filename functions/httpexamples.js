const functions = require('firebase-functions');
const admin = require('firebase-admin');

const db = admin.firestore();


//PATCH -> Bir kaydın kısmi olarak güncellenmesi için kullanılır.
//  İstekte sadece güncellenmesi istenen alanlar ve bu alanların yeni değerleri gönderilir.
//  Varolan bir kaydın sadece belirtilen alanları güncellenir, diğer alanlar dokunulmaz.
//  Veriyi eksiksiz göndermek yerine sadece değişiklik yapılan alanlar gönderilir.
exports.updateUserData = functions.https.onRequest(async (req, res) => {
    try {
      // Gelen veriyi ayrıştırın
      const userId = req.body.userId;
      const newData = req.body.data;
  
      // Veritabanına bağlanın ve kullanıcıyı güncelleyin
      const userRef = admin.firestore().collection('users').doc(userId);
      await userRef.update(newData);
  
      return res.status(200).json({ message: 'Kullanıcı verileri güncellendi.', body: req.body });
    } catch (error) {
      console.error('Hata:', error);
      return res.status(500).json({ error: 'Bir hata oluştu.' });
    }
  });

//DELETE
  exports.deleteDocument = functions.https.onRequest(async (req, res) => {
    if (req.method !== 'DELETE') {
      return res.status(405).send('Method Not Allowed'); // Yanıt: İzin verilmeyen metod
    }
  
    const docid = req.body.docid;
  
    if (!docid) {
      return res.status(400).send('Missing documentId'); // Yanıt: Belirtilen belge kimliği eksik
    }
  
    try {
      // Firestore veritabanından belgeyi silme
      await admin.firestore().collection('users').doc(docid).delete();
      res.status(400).send({ message: 'Kullanıcı verileri güncellendi.'}); // Başarılı yanıt, içerik olmadan (No Content)
    } catch (error) {
      console.error('Error deleting document:', error);
      res.status(500).send('Error deleting document');
    }
  });