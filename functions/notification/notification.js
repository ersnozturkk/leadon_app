const functions = require('firebase-functions');
const admin = require('firebase-admin');

const db = admin.firestore();

exports.UserFirstLogintrigger = functions.firestore
    .document("/users/{id}")
    .onCreate(async(snap, context) => {
      // eslint-disable-next-line no-undef
      await db.collection("users").where("username", "==", "admin888")
          .get()
          .then((snapshot) => {
            snapshot.forEach((doc) => {
              const userSnapshotToken = doc.data().usertoken;
              admin.messaging().sendToDevice(userSnapshotToken, {

                notification: {
                  title: "Yeni kullanıcı girişi",
                  body: "Konum : ",

                },
              });
            });
          });
    });

exports.Userupdateinfostrigger = functions.firestore
.document('users/{id}')
.onUpdate(async(change, context) => {
  const newUser = change.after.data().username;
  const previousUser = change.before.data().username;
  const imageURL = change.after.data().imagepath;
  if(newUser != previousUser){
    await db.collection("users").where("usertype", "==", "admin")
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        const userSnapshotToken = doc.data().usertoken;
        admin.messaging().sendToDevice(userSnapshotToken, {
          notification: {
            title: "Kullanıcı Bilgileri Güncellendi"  ,
            body: "Yeni Kullanıcı Adı :" + newUser,
            image: imageURL,
          },
        });
      });
    });
  }
});

exports.myHttpPostFunction = functions.https.onRequest((request, response) => {
  if (request.method === 'POST') {
    const requestData = request.body;

    try {
      const firestore = admin.firestore();
      const docRef = firestore.collection('collectionName').doc(); // Yeni bir belge referansı oluştur

    } catch (error) {
      
    }

    response.status(200).send(requestData);
  } else {
    response.status(400).send('Sadece POST istekleri kabul edilir.');
  }
});


exports.addUserToFirestore = functions.https.onRequest(async (request, response) => {
  if (request.method === 'POST') {
    const { ad, yaş } = request.body; // Gelen kullanıcı verisini al

    try {
      const firestore = admin.firestore();
      const usersCollection = firestore.collection('users'); // "users" koleksiyonuna referans oluştur

      // Yeni bir belge referansı oluştur ve kullanıcı verisini ekleyin
      await usersCollection.add({
        kullanıcı_adı: ad,
        yaş: yaş
      });

      response.status(200).send('Kullanıcı başarıyla Firestore\'a eklendi.');
    } catch (error) {
      response.status(500).send('Hata oluştu: ' + error);
    }
  } else {
    response.status(400).send('Sadece POST istekleri kabul edilir.');
  }
});