const admin = require("firebase-admin");
admin.initializeApp();

// User API
exports.usersAPI = require("./user");
exports.usersAPI2 = require("./services/user2");
exports.notifications = require("./notification/notification");
exports.HTTPexamples = require("./httpexamples");
