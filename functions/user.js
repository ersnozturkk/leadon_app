const functions = require('firebase-functions');
const admin = require('firebase-admin');

  exports.addUser = functions.https.onRequest(async (request, response) => {
  if (request.method === 'POST') {
    const { ad, yaş } = request.body; // Gelen kullanıcı verisini al

    try {
      const firestore = admin.firestore();
      const usersCollection = firestore.collection('users'); // "users" koleksiyonuna referans oluştur

      // Yeni bir belge referansı oluştur ve kullanıcı verisini ekleyin
      await usersCollection.add({
        kullanıcı_adı: ad,
        yaş: yaş
      });

      response.status(200).send('Kullanıcı başarıyla Firestore\'a eklendi.');
    } catch (error) {
      response.status(500).send('Hata oluştu: ' + error);
    }
  } else {
    response.status(400).send('Sadece POST istekleri kabul edilir.');
  }
});



exports.addUser2 = functions.https.onRequest(async (request, response) => {
  if (request.method === 'POST') {
    const { ad, yaş } = request.body; // Gelen kullanıcı verisini al

    try {
      const firestore = admin.firestore();
      const usersCollection = firestore.collection('users'); // "users" koleksiyonuna referans oluştur

      // Yeni bir belge referansı oluştur ve kullanıcı verisini ekleyin
      await usersCollection.add({
        kullanıcı_adı: ad,
        yaş: yaş
      });

      response.status(200).send('Kullanıcı başarıyla Firestore\'a eklendi.');
    } catch (error) {
      response.status(500).send('Hata oluştu: ' + error);
    }
  } else {
    response.status(400).send('Sadece POST istekleri kabul edilir.');
  }
});