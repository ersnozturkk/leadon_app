import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/firebase/users_table_api.dart';
import 'package:leadon_app/firebase_options.dart';
import 'package:leadon_app/modals/settings.dart';
import 'package:leadon_app/pages/homepage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:leadon_app/pages/initProvider.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';
import 'package:leadon_app/pages/register.dart';
import 'package:leadon_app/pages/versionCheckPage.dart';
import 'package:package_info_plus/package_info_plus.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  InitApp();

  if (auth.currentUser != null) {
    currentUser = await getCurrentUserFromFirebase(auth.currentUser!.uid);
    if (currentUser != null) currentuserlistenablevalue!.value = currentUser;
    // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
    currentuserlistenablevalue!.notifyListeners();
  }

  await getAllSettings();
  var isVersionCorrect = await checkAppVersion();
  runApp(App(isVersionCorrect: isVersionCorrect));
}

class App extends StatelessWidget {
  const App({Key? key, required this.isVersionCorrect}) : super(key: key);

  final bool isVersionCorrect;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
      home: isVersionCorrect == false ? VersionControlPage() : InitProvider(),
      title: 'Lead in',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.orange,
        fontFamily: GoogleFonts.nunito().fontFamily,
      ),
      routes: <String, WidgetBuilder>{
        '/LoginWithPhoneNumber': (BuildContext context) => LoginWithPhoneNumber(),
        '/Homepage': (BuildContext context) => HomePage(),
        '/Register': (BuildContext context) => Register(),
      },
    );
  }
}

void InitApp() async {
  currentuserlistenablevalue = ValueNotifier(currentUser);
  print('initApp');
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(alert: true, badge: true, sound: true);
  FirebaseMessaging.onMessage.listen((event) {
    print('Bildirim');
    print(event.notification!.title);
    print(event.notification!.body);
  });
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  Timer.periodic(Duration(seconds: 30), (timer) async {
    print('Timer main: Kullanıcı bilgileri guncelleniyor.');
    await updateCurrentUser();
  });
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  //updateCurrentUser();
  print('Bildirim : ${message.notification!.title} \n ${message.notification!.body}');
}

Future<bool> checkAppVersion() async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  var realAppversion = getSettingsValueAsString('androidversion').toString();
  return packageInfo.version.contains(realAppversion) ? true : false;
}
