import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

///WhatsApp's chat bubble type
///
///chat bubble color can be customized using [color]
///chat bubble tail can be customized  using [tail]
///chat bubble display message can be changed using [text]
///[text] is the only required parameter
///message sender can be changed using [isSender]
///chat bubble [TextStyle] can be customized using [textStyle]

class BubbleSpecialOne extends StatelessWidget {
  final bool isSender;
  final String text;
  final Timestamp time;
  final String user;
  final String userphotourl;
  final bool tail;
  final Color color;
  final bool sent;
  final bool delivered;
  final bool seen;
  final TextStyle textStyle;

  const BubbleSpecialOne({
    Key? key,
    this.isSender = true,
    required this.text,
    required this.time,
    required this.user,
    required this.userphotourl,
    this.color = Colors.white70,
    this.tail = true,
    this.sent = false,
    this.delivered = false,
    this.seen = false,
    this.textStyle = const TextStyle(
      color: Colors.black87,
      fontSize: 16,
    ),
  }) : super(key: key);

  ///chat bubble builder method
  @override
  Widget build(BuildContext context) {
    bool stateTick = false;
    Text? messagesenttime;
    if (sent) {
      stateTick = true;
      messagesenttime =
          Text(time.toDate().hour.toString() + ":" + time.toDate().minute.toString(), style: TextStyle(fontSize: 10, color: Colors.grey.shade300));
    }
    if (delivered) {
      stateTick = true;
      messagesenttime =
          Text(time.toDate().hour.toString() + ":" + time.toDate().minute.toString(), style: TextStyle(fontSize: 10, color: Colors.grey.shade300));
    }
    if (seen) {
      stateTick = true;
      messagesenttime =
          Text(time.toDate().hour.toString() + ":" + time.toDate().minute.toString(), style: TextStyle(fontSize: 10, color: Colors.grey.shade300));
    }

    return Align(
      alignment: isSender ? Alignment.topRight : Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (isSender == false)
              CircleAvatar(
                child: ClipOval(
                  child: CachedNetworkImage(
                    imageUrl: userphotourl,
                    fadeInDuration: Duration(milliseconds: 200),
                    fadeOutDuration: Duration(milliseconds: 200),
                    placeholder: (context, url) => Icon(Icons.person),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    cacheManager: CacheManager(Config('profileImage', stalePeriod: Duration(days: 2))),
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.cover,
                  ),
                ),
                radius: 22.5,
              ),
            CustomPaint(
              painter: SpecialChatBubbleOne(color: color, alignment: isSender ? Alignment.topRight : Alignment.topLeft, tail: tail),
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * .7,
                ),
                margin: isSender
                    ? stateTick
                        ? EdgeInsets.fromLTRB(7, 7, 14, 7)
                        : EdgeInsets.fromLTRB(7, 7, 17, 7)
                    : EdgeInsets.fromLTRB(17, 7, 7, 7),
                child: Column(
                  children: [
                    Stack(
                      children: <Widget>[
                        Padding(
                          padding: stateTick ? EdgeInsets.only(right: 32, top: 20) : EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                          child: Text(
                            text.length < 10 ? text.padRight(15 - text.length) : text,
                            style: textStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                        messagesenttime != null && stateTick
                            ? Positioned(
                                bottom: 0,
                                right: 0,
                                child: messagesenttime,
                              )
                            : SizedBox(
                                width: 1,
                              ),
                        Positioned(
                            right: 0,
                            top: 0,
                            child: Text(
                              user,
                              style: TextStyle(color: Colors.grey.shade800, fontWeight: FontWeight.w500),
                            ))
                      ],
                    ),
                  ],
                ),
              ),
            ),
            if (isSender == true)
              CircleAvatar(
                child: ClipOval(
                  child: CachedNetworkImage(
                    imageUrl: userphotourl,
                    fadeInDuration: Duration(milliseconds: 200),
                    fadeOutDuration: Duration(milliseconds: 200),
                    placeholder: (context, url) => Icon(Icons.person),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    cacheManager: CacheManager(Config('profileImage', stalePeriod: Duration(days: 2))),
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.cover,
                  ),
                ),
                radius: 22.5,
              ),
          ],
        ),
      ),
    );
  }
}

///custom painter use to create the shape of the chat bubble
///
/// [color],[alignment] and [tail] can be changed

class SpecialChatBubbleOne extends CustomPainter {
  final Color color;
  final Alignment alignment;
  final bool tail;

  SpecialChatBubbleOne({
    required this.color,
    required this.alignment,
    required this.tail,
  });

  double _radius = 10.0;
  double _x = 10.0;

  @override
  void paint(Canvas canvas, Size size) {
    if (alignment == Alignment.topRight) {
      if (tail) {
        canvas.drawRRect(
            RRect.fromLTRBAndCorners(
              0,
              0,
              size.width - _x,
              size.height,
              bottomLeft: Radius.circular(_radius),
              bottomRight: Radius.circular(_radius),
              topLeft: Radius.circular(_radius),
            ),
            Paint()
              ..color = this.color
              ..style = PaintingStyle.fill);
        var path = new Path();
        path.moveTo(size.width - _x, 0);
        path.lineTo(size.width - _x, 10);
        path.lineTo(size.width, 0);
        canvas.clipPath(path);
        canvas.drawRRect(
            RRect.fromLTRBAndCorners(
              size.width - _x,
              0.0,
              size.width,
              size.height,
              topRight: Radius.circular(3),
            ),
            Paint()
              ..color = this.color
              ..style = PaintingStyle.fill);
      } else {
        canvas.drawRRect(
            RRect.fromLTRBAndCorners(
              0,
              0,
              size.width - _x,
              size.height,
              bottomLeft: Radius.circular(_radius),
              bottomRight: Radius.circular(_radius),
              topLeft: Radius.circular(_radius),
              topRight: Radius.circular(_radius),
            ),
            Paint()
              ..color = this.color
              ..style = PaintingStyle.fill);
      }
    } else {
      if (tail) {
        canvas.drawRRect(
            RRect.fromLTRBAndCorners(
              _x - 0.1,
              0,
              size.width,
              size.height,
              bottomRight: Radius.circular(_radius),
              topRight: Radius.circular(_radius),
              bottomLeft: Radius.circular(_radius),
            ),
            Paint()
              ..color = this.color
              ..style = PaintingStyle.fill);
        var path = new Path();
        path.moveTo(_x, 0);
        path.lineTo(_x, 10);
        path.lineTo(0, 0);
        canvas.clipPath(path);
        canvas.drawRRect(
            RRect.fromLTRBAndCorners(
              0,
              0.0,
              _x,
              size.height,
              topLeft: Radius.circular(3),
            ),
            Paint()
              ..color = this.color
              ..style = PaintingStyle.fill);
      } else {
        canvas.drawRRect(
            RRect.fromLTRBAndCorners(
              _x,
              0,
              size.width,
              size.height,
              bottomRight: Radius.circular(_radius),
              topRight: Radius.circular(_radius),
              bottomLeft: Radius.circular(_radius),
              topLeft: Radius.circular(_radius),
            ),
            Paint()
              ..color = this.color
              ..style = PaintingStyle.fill);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
