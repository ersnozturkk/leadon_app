import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Text headline6(BuildContext context, String text, [Color color = Colors.black]) => Text(text,
    textAlign: TextAlign.center,
    style: TextStyle(
      fontSize: 24,
      fontFamily: GoogleFonts.nunito().fontFamily,
    ));

Widget smallHeadline(BuildContext context, String text, [Color color = Colors.black]) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: AutoSizeText(
      text,
      textAlign: TextAlign.start,
      style: GoogleFonts.nunito(
        textStyle: TextStyle(fontWeight: FontWeight.w800),
      ),
      maxLines: 1,
    ),
  );
}
