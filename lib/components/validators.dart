var mailvalidate = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
var usernamevalidate = RegExp(r"^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$");
var namevalidator = RegExp(r"^\s*([A-Za-zğüşöçıİĞÜŞÖÇ]{1,}([\.,] |[-']| ))+[A-Za-zğüşöçıİĞÜŞÖÇ]+\.?\s*$");

var channelnamevalidate = RegExp(r"^(?=[a-zA-Z0-9._ ]{5,20}$)(?!.*[ _.]{2})[^_.].*[^_.]$");
var channeldescriptionvalidate = RegExp(r"^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$");
