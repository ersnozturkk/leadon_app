import 'package:flutter/material.dart';
import 'package:leadon_app/modals/liadInPlaces.dart';
import 'package:leadon_app/pages/homepage.dart';
import 'package:leadon_app/pages/placepage/items.dart';
import 'package:leadon_app/pages/placepage/placepageBodies.dart';
import 'package:leadon_app/pages/searchpage.dart';
import 'package:leadon_app/pages/userProfile.dart';
import 'package:leadon_app/pages/userhistory.dart';

Map<String, Widget> PlacesPageTabAndBodies(BuildContext context, LeadInPlaces currentplace, ScrollController scrollController) => {
      'İçerik': contentBody(currentplace: currentplace, scroolcontroller: scrollController),
      'Duvar': DuvarBody(placedocid: currentplace.placeid!),
      'Sohbet': ChannelListPage(currentplace: currentplace, scrollcontroller: scrollController, channeltype: 'chat'),
      //tabbarItem(context, 'Oyunlar'): ChannelListPage(currentplace: currentplace, scrollcontroller: scrollController, channeltype: 'game'),
      'Etkinlikler': EventsPageBody(currentplace: currentplace, scrollcontroller: scrollController, channeltype: 'chat'),
      'Bize Ulaş': ContactPageBody(currentplace: currentplace, scrollcontroller: scrollController, channeltype: 'chat'),
    };

Map<BottomNavigationBarItem, Widget> HomePageTabAndBodies() => {
      BottomNavigationButtonItem('Anasayfa', Icons.home): HomepageBody2(),
      BottomNavigationButtonItem('Keşfet', Icons.search): SearchPlacePage(),
      BottomNavigationButtonItem('Geçmiş', Icons.timer_outlined): UserMessagesPage(),
      BottomNavigationButtonItem('Hesabım', Icons.person): ProfilePageBody(),
    };
Widget tabbarItem(BuildContext context, String title) {
  return Column(
    children: [
      Container(
          width: (MediaQuery.of(context).size.width - 20) / 4,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          decoration: BoxDecoration(color: Colors.orange, borderRadius: BorderRadius.circular(10)),
          child: Tab(text: title)),
    ],
  );
}
