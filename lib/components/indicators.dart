import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void showindicator(BuildContext context, String text) {
  showDialog(
      barrierDismissible: false,
      useSafeArea: false,
      context: context,
      builder: (context) {
        return defaultProgressIndicator(text);
      });
}

void closeindicatorOrPreviousPage(BuildContext context) => Navigator.pop(context);

Widget defaultProgressIndicator(String text) {
  return WillPopScope(
    onWillPop: () async => false,
    child: Container(
      color: Colors.indigoAccent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AnimatedTextKit(
            animatedTexts: [
              TypewriterAnimatedText(
                text,
                textAlign: TextAlign.center,
                cursor: '.',
                textStyle: GoogleFonts.nunito(
                  fontSize: 32,
                  color: Colors.black,
                  decoration: TextDecoration.none,
                  fontWeight: FontWeight.w800,
                ),
                speed: const Duration(milliseconds: 30),
              ),
            ],
            totalRepeatCount: 1000,
            pause: const Duration(milliseconds: 30),
            displayFullTextOnTap: false,
            stopPauseOnTap: false,
          ),
          Center(
              child: Image.asset(
            'assets/launchericon.png',
            width: 130,
            height: 130,
          )),
        ],
      ),
    ),
  );
}
