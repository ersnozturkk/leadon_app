import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/components/text.dart';
import 'package:leadon_app/firebase/channels_api.dart';
import 'package:leadon_app/modals/GoogleAutoComplatePlaces.dart';
import 'package:leadon_app/modals/GoogleNearlyPlaces.dart';
import 'package:leadon_app/modals/liadInPlaces.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';
import 'package:leadon_app/pages/place.dart';

/**
 * Place'lerden 2 tanesi aynı anda null olamaz.
*/
Widget LocationResultBar(BuildContext context, String query, Color background, [Result? place, Prediction? place2]) {
  var placeid = place != null ? place.placeId : place2!.placeId;
  var placename = place != null ? place.name! : place2!.structuredFormatting!.mainText!;
  var placeaddress = place != null ? place.vicinity.toString() : place2!.structuredFormatting!.secondaryText!;
  var placelocation = place != null ? getDistance(place.geometry!.location!, currentUser!.location!) : "-";
  return Container(
    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
    height: 90,
    child: MaterialButton(
      elevation: 5,
      splashColor: Colors.yellow,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      color: background,
      child: ListTile(
          minLeadingWidth: 0.2,
          leading: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.location_on),
              Text(placelocation),
            ],
          ),
          title: smallHeadline(
            context,
            placename,
          ),
          subtitle: Text(
            placeaddress,
            maxLines: 2,
          )),
      onPressed: () async {
        print('Mekana Git');
        var currentplace = await getCurrentPlace(placeid!);
        if (currentplace == null) {
          showindicator(context, 'Merdivenlere dikkat et mekana giriyoruz..');
          LeadInPlaces newPlace;
          if (place != null) {
            newPlace = LeadInPlaces(
              channels: [],
              name: place.name,
              description: place.vicinity!,
              placeid: place.placeId,
              types: place.types,
              location: GeoPoint(place.geometry!.location!.lat!, place.geometry!.location!.lng!),
              features: [],
            );
          } else {
            newPlace = LeadInPlaces(
              channels: [],
              name: place2!.structuredFormatting!.mainText,
              description: place2.structuredFormatting!.secondaryText!,
              placeid: place2.placeId,
              types: place2.types,
              location: null,
              features: [],
            );
          }

          await addnewPlaceandDefaultChannels(newPlace);
          currentplace = await getCurrentPlace(newPlace.placeid!);
          closeindicatorOrPreviousPage(context);
        }
        setVisitorToPlace(currentplace!, false);
        setVisitedPlaceToUser(currentplace, false);

        closeindicatorOrPreviousPage(context);
        Navigator.push(context, MaterialPageRoute(builder: (context) => PlacePage(place: currentplace!)));
      },
    ),
  );
}

String getDistance(Location placelocation, GeoPoint userLocation) {
  var distance = Geolocator.distanceBetween(
    placelocation.lat!,
    placelocation.lng!,
    userLocation.latitude,
    userLocation.longitude,
  ).round();

  return distance < 1000 ? distance.toString() + " m" : (distance / 1000).round().toString() + " km";
}

Future<LeadInPlaces?> getCurrentPlace(String placeid) async {
  var result = await FirebaseFirestore.instance.collection('places').doc(placeid).get();

  if (result.data() != null) return LeadInPlaces.fromJson(result.data()!, result.id);

  return null;
}
