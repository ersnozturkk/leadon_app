import 'package:flutter/material.dart';

void showasnackbar(BuildContext context, String text, MaterialColor color, Function(SnackBarClosedReason) action) {
  final snackBar = SnackBar(
      dismissDirection: DismissDirection.up,
      elevation: 0,
      duration: Duration(milliseconds: 1500),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
      backgroundColor: Colors.transparent,
      content: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          gradient: LinearGradient(
            colors: [
              color.shade800,
              color.shade300,
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            text,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ));
  ScaffoldMessenger.of(context).showSnackBar(snackBar).closed.then(action);
}
