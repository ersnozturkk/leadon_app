import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:leadon_app/modals/story.dart';
import 'package:story_view/story_view.dart';

class StoriesWidget extends StatefulWidget {
  const StoriesWidget({super.key, @required this.stories});

  final List<Story>? stories;

  @override
  State<StoriesWidget> createState() => _StoriesWidgetState();
}

class _StoriesWidgetState extends State<StoriesWidget> {
  @override
  Widget build(BuildContext context) {
    List<Story> stories = widget.stories!;
    final controller = StoryController();

    return Container(
      height: 125,
      decoration: BoxDecoration(color: Colors.transparent),
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: stories.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Colors.deepOrange,
                            width: 2,
                          )),
                      child: CircleAvatar(
                        radius: (MediaQuery.of(context).size.width - 10) / 12,
                        backgroundImage: NetworkImage(stories[index].filename!),
                        backgroundColor: Colors.blueAccent,
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    AutoSizeText(
                      stories[index].text!.replaceAll(" ", "\n"),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
                onTap: () {
                  showStoryContent(context, stories[index].content!, controller);
                },
              ),
            );
          }),
    );
  }
}

void showStoryContent(BuildContext context, String scontent, StoryController controller) {
  var content = getstorycontents(scontent, controller);
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return StoryView(
        storyItems: content,
        progressPosition: ProgressPosition.top,
        repeat: false,
        inline: true,
        controller: controller, // pass controller here too
        onStoryShow: (s) {},
        onComplete: () {
          Navigator.pop(context);
        },
        onVerticalSwipeComplete: (direction) {
          if (direction == Direction.down) {
            Navigator.pop(context);
          }
        },
      );
    },
  );
}

List<StoryItem> getstorycontents(String contentlist, StoryController controller) {
  var urllist = contentlist.split(",");
  List<StoryItem> items = [];
  for (var i = 0; i < urllist.length; i++) {
    items.add(StoryItem.pageImage(url: urllist[i], controller: controller));
  }
  return items;
}
