import 'dart:async';

import 'package:geolocator/geolocator.dart';

final LocationSettings locationSettings = LocationSettings(
  accuracy: LocationAccuracy.high,
  distanceFilter: 100,
);
StreamSubscription<Position> positionStream = Geolocator.getPositionStream(locationSettings: locationSettings).listen((Position? position) {
  print(position == null ? 'Unknown' : '${position.latitude.toString()}, ${position.longitude.toString()}');
});

Future<bool> checkLocationnPermission() async {
  var permission = await Geolocator.checkPermission();

  if (permission == LocationPermission.always || permission == LocationPermission.whileInUse) {
    print("Konuma izin verildi.");
    return true;
  }
  print("Konuma izin verilmedi.");

  return false;
}
