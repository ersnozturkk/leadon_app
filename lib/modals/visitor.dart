import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

Visitor visitedchannelFromJson(String str, String userdocid) => Visitor.fromJson(json.decode(str), userdocid);

String visitedchannelToJson(Visitor data) => json.encode(data.toJson());

class Visitor {
  Visitor({
    this.visittime,
    this.userid,
    this.visitcount,
  });

  Timestamp? visittime;
  String? userid;
  int? visitcount;

  factory Visitor.fromJson(Map<String, dynamic> json, String userdocid) => Visitor(
        visittime: json["visittime"] == null ? null : json["visittime"],
        visitcount: json["visitcount"] == null ? null : int.tryParse(json["visittime"]),
        userid: userdocid,
      );

  Map<String, dynamic> toJson() => {
        "visittime": visittime == null ? null : visittime,
        "visitcount": visitcount == null ? null : visitcount,
      };
}
