import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leadon_app/modals/ChannelMessages.dart';

Channel placeChannelsFromJson(String str, String docid) => Channel.fromJson(json.decode(str), docid);

String placeChannelsToJson(Channel data) => json.encode(data.toJson());

class Channel {
  Channel({this.description, this.shortdescription, this.imageurl, this.channelid, this.type, this.name, this.createdTime});

  String? description;
  String? shortdescription;
  String? imageurl;
  String? type;
  String? name;
  String? channelid;
  Timestamp? createdTime;
  List<ChannelMessages>? messages = [];

  factory Channel.fromJson(Map<String, dynamic> json, String channelid) => Channel(
      description: json["description"] == null ? null : json["description"],
      imageurl: json["imageurl"] == null ? null : json["imageurl"],
      shortdescription: json["shortdescription"] == null ? null : json["shortdescription"],
      type: json["type"] == null ? null : json["type"],
      name: json["name"] == null ? null : json["name"],
      createdTime: json["createdTime"] == null ? null : json["createdTime"],
      channelid: channelid);

  Map<String, dynamic> toJson() => {
        "description": description == null ? null : description,
        "imageurl": imageurl == null ? null : imageurl,
        "shortdescription": shortdescription == null ? null : shortdescription,
        "type": type == null ? 'Unknow' : type,
        "name": name == null ? null : name,
        "createdTime": createdTime == null ? Timestamp.fromDate(DateTime.now()) : createdTime,
      };
}
