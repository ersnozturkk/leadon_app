import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Input extends StatelessWidget {
  final onTap;
  final onChanged;
  final validator;
  final int? maxlenght;
  final TextEditingController? controller;
  final bool? autofocus;
  final bool obscureText;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final String? placeholder;
  final String? errortext;
  final String? labeltext;
  final Color borderColor;
  final double borderwidth;
  final GlobalKey? formkey;
  final TextInputType? keyboardType;
  Input(
      {this.onTap,
      this.onChanged,
      this.controller,
      this.autofocus = false,
      this.obscureText = false,
      this.suffixIcon,
      this.prefixIcon,
      this.placeholder,
      this.borderColor = Colors.blue,
      this.borderwidth = 2,
      this.errortext,
      this.validator,
      this.maxlenght,
      this.formkey,
      this.keyboardType,
      this.labeltext});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        key: formkey,
        child: TextFormField(
          textInputAction: TextInputAction.next,
          maxLength: maxlenght,
          minLines: 1,
          maxLines: 5,
          keyboardType: keyboardType,
          validator: validator,
          cursorColor: Colors.blue,
          onTap: onTap,
          onChanged: onChanged,
          controller: controller,
          autofocus: autofocus!,
          obscureText: obscureText,
          style: TextStyle(height: 1, fontSize: 17.5, color: Colors.grey),
          textAlignVertical: TextAlignVertical(y: 0.9),
          decoration: InputDecoration(
              labelText: labeltext,
              prefixIconColor: Colors.blue,
              focusColor: Colors.blue,
              hoverColor: Colors.blue,
              errorBorder: InputBorder.none,
              errorStyle: TextStyle(),
              errorText: errortext,
              fillColor: Colors.white,
              hintStyle: TextStyle(
                color: Colors.blueGrey,
              ),
              suffixIcon: suffixIcon,
              prefixIcon: prefixIcon,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
                borderSide: BorderSide(color: borderColor, width: borderwidth, style: BorderStyle.solid),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
                borderSide: BorderSide(color: borderColor, width: borderwidth, style: BorderStyle.solid),
              ),
              hintText: placeholder),
        ),
      ),
    );
  }
}

class Input2 extends StatelessWidget {
  final onTap;
  final onChanged;
  final validator;
  final int? maxlenght;
  final TextEditingController? controller;
  final bool? autofocus;
  final bool obscureText;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final String? placeholder;
  final String? errortext;
  final String? labeltext;
  final Color borderColor;
  final double borderwidth;
  final GlobalKey? formkey;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  Input2(
      {this.onTap,
      this.onChanged,
      this.controller,
      this.autofocus = false,
      this.obscureText = false,
      this.suffixIcon,
      this.prefixIcon,
      this.placeholder,
      this.borderColor = Colors.blue,
      this.borderwidth = 2,
      this.errortext,
      this.validator,
      this.maxlenght,
      this.formkey,
      this.keyboardType,
      this.textInputAction,
      this.labeltext});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 8, 30, 8),
      child: Form(
        key: formkey,
        child: TextFormField(
          textInputAction: textInputAction == null ? TextInputAction.next : textInputAction,
          maxLength: maxlenght,
          minLines: 1,
          maxLines: 5,
          keyboardType: keyboardType,
          validator: validator,
          cursorColor: Colors.deepOrange,
          onTap: onTap,
          onChanged: onChanged,
          controller: controller,
          autofocus: autofocus!,
          obscureText: obscureText,
          style: GoogleFonts.nunito(height: 1, fontSize: 17.5, color: Colors.grey),
          textAlignVertical: TextAlignVertical(y: 0.9),
          decoration: InputDecoration(
              labelText: labeltext,
              labelStyle: GoogleFonts.nunito(color: Colors.black54, fontSize: 18),
              prefixIconColor: Colors.blue,
              focusColor: Colors.blue,
              hoverColor: Colors.blue,
              errorStyle: TextStyle(),
              errorText: errortext,
              fillColor: Colors.white,
              hintStyle: TextStyle(
                color: Colors.blueGrey,
              ),
              suffixIcon: suffixIcon,
              prefixIcon: prefixIcon,
              hintText: placeholder),
        ),
      ),
    );
  }
}

String getFixedPhoneNumber(String phoneNumber) {
  phoneNumber = phoneNumber.replaceFirstMapped(RegExp(r".{3}"), (match) => "${match.group(0)} ");
  phoneNumber = phoneNumber.replaceFirstMapped(RegExp(r".{8}"), (match) => "${match.group(0)} ");
  phoneNumber = phoneNumber.replaceFirstMapped(RegExp(r".{12}"), (match) => "${match.group(0)} ");
  return phoneNumber;
}
