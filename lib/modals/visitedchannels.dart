import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

Visitedchannel visitedchannelFromJson(String str, String channeldocid) => Visitedchannel.fromJson(json.decode(str), channeldocid);

String visitedchannelToJson(Visitedchannel data) => json.encode(data.toJson());

class Visitedchannel {
  Visitedchannel({this.visittime, this.placeid, this.channelid, this.placename, this.channelname, this.visitcount});

  Timestamp? visittime;
  String? placeid;
  String? placename;
  String? channelname;
  String? channelid;
  int? visitcount;

  factory Visitedchannel.fromJson(Map<String, dynamic> json, String channeldocid) {
    return Visitedchannel(
      visittime: json["visittime"] == null ? null : json["visittime"],
      visitcount: int.tryParse(json["visitcount"].toString()) == null ? null : int.parse(json["visitcount"].toString()),
      placeid: json["placeid"] == null ? null : json["placeid"],
      placename: json["placename"] == null ? null : json["placename"],
      channelname: json["channelname"] == null ? null : json["channelname"],
      channelid: channeldocid,
    );
  }

  Map<String, dynamic> toJson() => {
        "visitcount": visitcount == null ? null : visitcount,
        "visittime": visittime == null ? null : visittime,
        "placeid": placeid == null ? null : placeid,
        "placename": placename == null ? null : placename,
        "channelname": channelname == null ? null : channelname,
      };
}
