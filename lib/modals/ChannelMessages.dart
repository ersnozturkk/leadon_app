import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

ChannelMessages channelMessagesFromJson(String str) => ChannelMessages.fromJson(json.decode(str));

String channelMessagesToJson(ChannelMessages data) => json.encode(data.toJson());

class ChannelMessages {
  ChannelMessages({
    this.message,
    this.username,
    required this.photourl,
    this.senttime,
  });

  String? message;
  String? username;
  String? photourl;
  Timestamp? senttime;

  factory ChannelMessages.fromJson(Map<String, dynamic> json) => ChannelMessages(
        message: json["message"] == null ? null : json["message"],
        username: json["username"] == null ? null : json["username"],
        photourl: json["photourl"] == null ? null : json["photourl"],
        senttime: json["senttime"] == null ? null : json["senttime"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "username": username == null ? null : username,
        "photourl": photourl == null ? null : photourl,
        "senttime": senttime == null ? null : senttime,
      };
}
