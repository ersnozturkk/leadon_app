// To parse this JSON data, do
//
// final setting = settingFromJson(jsonString);

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

Setting settingFromJson(String str) => Setting.fromJson(json.decode(str));

String settingToJson(Setting data) => json.encode(data.toJson());

List<Setting> settings = [];

class Setting {
  Setting({
    this.name,
    this.value,
  });

  String? name;
  String? value;

  factory Setting.fromJson(Map<String, dynamic> json) => Setting(
        name: json["name"] == null ? null : json["name"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "value": value == null ? null : value,
      };
}

String? getSettingsValueAsString(String settingname) {
  try {
    var searchingsetting = settings.firstWhere((element) => element.name == settingname);
    return searchingsetting.value!;
  } catch (e) {
    return null;
  }
}

int? getSettingsValueAsNumber(String settingname) {
  try {
    var searchingsetting = settings.firstWhere((element) => element.name == settingname);
    return int.parse(searchingsetting.value!);
  } catch (e) {
    return null;
  }
}

Future<void> getAllSettings() async {
  settings.clear();
  var snapshot = await FirebaseFirestore.instance.collection('settings').get();
  print('Toplamdaaa ${snapshot.size} ayar alındı.');

  snapshot.docs.forEach((element) {
    settings.add(Setting.fromJson(element.data()));
  });
}
