import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

Visitedplaces visitedchannelFromJson(String str, String placedocid) => Visitedplaces.fromJson(json.decode(str), placedocid);

String visitedchannelToJson(Visitedplaces data) => json.encode(data.toJson());

class Visitedplaces {
  Visitedplaces({this.visittime, this.placeid, this.visitcount, this.placename});

  Timestamp? visittime;
  String? placeid;
  String? placename;
  int? visitcount;

  factory Visitedplaces.fromJson(Map<String, dynamic> json, String placedocid) => Visitedplaces(
        visittime: json["visittime"] == null ? null : json["visittime"],
        visitcount: json["visitcount"] == null ? null : json["visitcount"],
        placename: json["placename"] == null ? null : json["placename"],
        placeid: placedocid,
      );

  Map<String, dynamic> toJson() => {
        "visittime": visittime == null ? null : visittime,
        "visitcount": visitcount == null ? null : visitcount,
        "placename": placename == null ? null : placename,
      };
}
