import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/visitor.dart';

LeadInPlaces leadInPlacesFromJson(String str, String placeid) => LeadInPlaces.fromJson(json.decode(str), placeid);

String leadInPlacesToJson(LeadInPlaces data) => json.encode(data.toJson());

class LeadInPlaces {
  LeadInPlaces({this.features, this.types, this.imageurl, this.name, this.placeid, this.location, this.description, this.visittime, required this.channels});

  List<String>? features;
  List<String>? types;
  String? name;
  String? placeid;
  String? description;
  String? imageurl;
  GeoPoint? location;
  DateTime? visittime;
  List<Channel> channels = [];
  List<Visitor> visitors = [];
  factory LeadInPlaces.fromJson(Map<String, dynamic> json, String placeid) {
    return LeadInPlaces(
      //features: json["features"] == null ? null : List<String>.from(json["features"].map((x) => x)),
      //types: json["types"] == null ? null : List<String>.from(json["types"].map((x) => x)),
      name: json["name"] == null ? null : json["name"],
      placeid: placeid,
      description: json["description"] == null ? null : json["description"],
      imageurl: json["imageurl"] == null ? null : json["imageurl"],
      location: json["location"] == null ? null : json["location"],
      visittime: json["visittime"] == null ? null : json["visittime"],
      channels: [],
    );
  }

  Map<String, dynamic> toJson() => {
        //"features": features == null ? null : List<dynamic>.from(features!.map((x) => x)),
        //"types": types == null ? null : List<dynamic>.from(types!.map((x) => x)),
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "imageurl": imageurl == null ? null : imageurl,
        "location": location == null ? null : location,
      };
}
