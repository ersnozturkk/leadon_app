// To parse this JSON data, do
//
//     final story = storyFromJson(jsonString);

import 'dart:convert';

Story storyFromJson(String str) => Story.fromJson(json.decode(str));

String storyToJson(Story data) => json.encode(data.toJson());

class Story {
  Story({
    this.borderColor,
    this.filename,
    this.text,
    this.content,
  });

  String? borderColor;
  String? filename;
  String? text;
  String? content;

  factory Story.fromJson(Map<String, dynamic> json) {
    return Story(
      borderColor: json["borderColor"] == null ? null : json["borderColor"],
      filename: json["filename"] == null ? null : json["filename"],
      text: json["text"] == null ? null : json["text"],
      content: json["content"] == null ? null : json["content"],
    );
  }

  Map<String, dynamic> toJson() => {
        "borderColor": borderColor == null ? null : borderColor,
        "filename": filename == null ? null : filename,
        "text": text == null ? null : text,
        "content": content == null ? null : content,
      };
}
