import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leadon_app/modals/visitedchannels.dart';
import 'package:leadon_app/modals/visitedplaces.dart';

List<LUser> UserFromJson(String str, String userdocid) => List<LUser>.from(json.decode(str).map((x) => LUser.fromJson(x, userdocid)));

String UserToJson(List<LUser> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LUser {
  LUser({
    this.uid,
    this.age,
    this.city,
    this.horoscope,
    this.job,
    this.mail,
    this.name,
    this.tel,
    this.username,
    this.usertype,
    this.lastseen,
    this.location,
    this.createdtime,
    this.usertoken,
    this.imagepath,
    this.smallimagepath,
  });
  String? uid;
  String? age;
  String? city;
  String? horoscope;
  String? job;
  String? mail;
  String? name;
  String? tel;
  String? username;
  String? usertype;
  String? usertoken;
  GeoPoint? location;
  String? imagepath;
  String? smallimagepath;
  Timestamp? lastseen;
  Timestamp? createdtime;
  //Other collection
  Visitedchannel? visitedchannel;
  Visitedplaces? visitedplaces;

  factory LUser.fromJson(Map<String, dynamic> json, String userdocid) {
    return LUser(
      uid: json["uid"],
      age: json["age"],
      city: json["city"],
      horoscope: json["horoscope"],
      job: json["job"],
      location: json["location"],
      mail: json["mail"],
      name: json["name"],
      tel: json["tel"],
      username: json["username"],
      usertype: json["usertype"],
      usertoken: json["usertoken"],
      imagepath: json["imagepath"],
      lastseen: json["lastseen"],
      createdtime: json["createdtime"],
    );
  }

  Map<String, dynamic> toJson() => {
        "age": age,
        "city": city,
        "horoscope": horoscope,
        "job": job,
        "mail": mail,
        "name": name,
        "tel": tel,
        "username": username,
        "usertype": usertype,
        "imagepath": imagepath,
        "usertoken": usertoken,
        "lastseen": Timestamp.fromDate(DateTime.now()),
        "createdtime": Timestamp.fromDate(DateTime.now()),
        "location": location,
      };
}
