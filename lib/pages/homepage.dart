import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/buttons.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/components/storiesWidget.dart';
import 'package:leadon_app/components/tabbaritems.dart';
import 'package:leadon_app/components/text.dart';
import 'package:leadon_app/firebase/general_api.dart';
import 'package:leadon_app/modals/GoogleNearlyPlaces.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/story.dart';
import 'package:leadon_app/pages/place.dart';
import 'package:leadon_app/pages/searchpage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

ValueNotifier<String>? __scanedQrListener;

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  String _scanedBarcode = 'Unknow';

  TabController? controller;
  ValueNotifier<int>? __currentIndex;

  @override
  void initState() {
    super.initState();
    __scanedQrListener = ValueNotifier<String>(_scanedBarcode);
    controller = TabController(length: HomePageTabAndBodies().values.length, vsync: this);
    __currentIndex = ValueNotifier<int>(controller!.index);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: DefaultTabController(
        initialIndex: 0,
        length: HomePageTabAndBodies().values.length,
        child: Scaffold(
          appBar: LeadInDefaultAppbar(context),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            clipBehavior: Clip.none,
            controller: controller,
            children: HomePageTabAndBodies().values.toList(),
          ),
          bottomNavigationBar: ValueListenableBuilder(
            valueListenable: __currentIndex!,
            builder: (context, value, child) {
              return BottomNavigationBar(
                backgroundColor: Colors.grey.shade50,
                fixedColor: Colors.deepOrange,
                unselectedItemColor: Colors.grey,
                currentIndex: __currentIndex!.value,
                onTap: (newIndex) {
                  print("NewIndex: " + newIndex.toString());
                  __currentIndex!.value = newIndex;
                  controller!.index = newIndex;
                },
                items: HomePageTabAndBodies().keys.toList(),
              );
            },
          ),
        ),
      ),
    );
  }
}

AppBar LeadInDefaultAppbar(BuildContext context) {
  return AppBar(
    automaticallyImplyLeading: false,
    backgroundColor: Colors.deepOrangeAccent,
    centerTitle: true,
    titleTextStyle: GoogleFonts.shadowsIntoLight(fontWeight: FontWeight.w700, color: Colors.black, fontSize: 24),
    title: Text("Leadİn"),
    elevation: 0,
  );
}

Future<String?> scanPlaceQR() async {
  String barcodeScanRes;

  barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('#479674', 'Geri', false, ScanMode.QR);

  //if (!mounted) return; global yapabilmek için silindi. Hata çıkarsa bak
  print("Ersin" + barcodeScanRes);

  __scanedQrListener!.value = barcodeScanRes;
  if (barcodeScanRes.contains("placeqr")) {
    print('QR Okundu abicim');
    int rawdatastartindex = barcodeScanRes.indexOf('placeqr=') + 8;
    int rawdataendindex = barcodeScanRes.indexOf('FF');
    print("Start index= " + rawdatastartindex.toString());
    print("End index= " + rawdataendindex.toString());

    if (rawdataendindex == -1 || rawdatastartindex == -1 || rawdatastartindex > rawdataendindex) return null;

    barcodeScanRes = barcodeScanRes.substring(rawdatastartindex, rawdataendindex);
    print("Barcode scan result:  " + barcodeScanRes);
    return barcodeScanRes;
  } else {
    return null;
  }
}

class HomepageBody2 extends StatefulWidget {
  const HomepageBody2({super.key});

  @override
  State<HomepageBody2> createState() => _HomepageBody2State();
}

TextEditingController searchBarTextController = TextEditingController();

class _HomepageBody2State extends State<HomepageBody2> with AutomaticKeepAliveClientMixin<HomepageBody2> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Material(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              headline6(context, 'Qr kod\'u görüyorsan okut yada bulunduğun mekanı ara bul'),
              MySearchBar(context, searchBarTextController),
              HomePageStoriesListWidget(),
              smallHeadline(context, 'LeadIn Bahçe Sohbetleri'),
              FutureBuilder(
                future: getCategoryTemplates('chat'),
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data!.length > 0)
                    return channelTemplatesWidget(
                      axis: Axis.horizontal,
                      channels: snapshot.data!,
                    );

                  return CircularProgressIndicator();
                },
              ),
              smallHeadline(context, 'LeadIn Bahçe'),
              FutureBuilder(
                future: getCategoryTemplates('game'),
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data!.length > 0)
                    return channelTemplatesWidget(
                      axis: Axis.vertical,
                      channels: snapshot.data!,
                    );

                  return CircularProgressIndicator();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

class HomePageStoriesListWidget extends StatelessWidget {
  const HomePageStoriesListWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getStories(),
        builder: (context, AsyncSnapshot<List<Story>> snapshot) {
          if (snapshot.hasData) {
            var storylist = snapshot.data;
            return StoriesWidget(stories: storylist!);
          }
          return Container(
            height: 110,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(color: Colors.grey.shade100),
            child: Center(child: CircularProgressIndicator()),
          );
        });
  }
}

class channelTemplatesWidget extends StatelessWidget {
  const channelTemplatesWidget({Key? key, required this.axis, required this.channels}) : super(key: key);

  final Axis? axis;
  final List<Channel> channels;
  @override
  Widget build(BuildContext context) {
    double mainAxisExtent = axis == Axis.vertical ? 300 : 200;
    double? containerheight;
    if (axis == Axis.vertical) {
      containerheight = mainAxisExtent * (channels.length.toDouble() + 1) / 2;
    } else {
      containerheight = mainAxisExtent * 2;
    }
    return Container(
      height: containerheight,
      child: GridView.builder(
        scrollDirection: axis!,
        addAutomaticKeepAlives: false,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: axis == Axis.vertical ? 2 : 1,
          mainAxisExtent: axis == Axis.vertical ? mainAxisExtent : null,
          childAspectRatio: 1.7,
        ),
        physics: axis == Axis.vertical ? NeverScrollableScrollPhysics() : null,
        itemCount: channels.length,
        itemBuilder: (context, index) {
          return Card(
            shape: ContinuousRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            surfaceTintColor: Colors.red,
            elevation: 22,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      channels[index].imageurl!,
                    ),
                  )),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      channels[index].shortdescription!,
                      style: GoogleFonts.nunito(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    Container(
                      decoration: BoxDecoration(color: Colors.deepOrange, borderRadius: BorderRadius.circular(8)),
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 40 - channels[index].name!.length.toDouble()),
                        child: Text(
                          channels[index].name!,
                          style: GoogleFonts.nunito(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class MySearchDelegate extends SearchDelegate {
  String get searchFieldLabel => 'QR kodu okut yada Mekan Ara...';
  TextStyle get searchFieldStyle => TextStyle(fontSize: 15);
  @override
  List<Widget>? buildActions(BuildContext context) {
    print("arama sayfası açık");

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          searchBarTextController.text = '';
          close(context, null);
          FocusScope.of(context).unfocus();
          print("Temizle...");
        },
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return qrCodeIcon();
  }

  @override
  Widget buildResults(BuildContext context) {
    print("aranıyorr" + query);
    List<String> matchQuery = [];

    if (query.length == 0) {
      return ListTile(
        title: Text("Aramaya devam et"),
      );
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(matchQuery[index]),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return FutureBuilder<Response<String>>(
      future: getNearbylacesFromGooglePlacesAPI(query),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          GoogleNearlyPlace places = googleNearlyPlaceFromJson(snapshot.data!.data!);

          /*if (query.length == 0) {
            return Center(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  mainText('Sana Yakan Gece Mekanları'),
                  mainText('Yemelik İçmelik Mekanlar'),
                ],
              ),
            );
          }*/
          /*if (query.length < 3) {
            return Center(child: CircularProgressIndicator());
          }*/
          return Center(
              child: ListView.builder(
            itemCount: places.results!.length,
            itemBuilder: (context, index) {
              return LocationResultBar(context, query, Colors.orangeAccent, places.results![index]);
            },
          ));
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}

Widget CategoryTemplateItems([List<Channel>? templates]) {
  // var textBackgroundColor = templates[0].type == 'chat' ? Colors.orangeAccent : Colors.green;
  return Material(
    child: Expanded(
      flex: 1,
      child: GridView.builder(
        cacheExtent: 9999,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: 10,
        itemBuilder: (context, index) {
          return Text('hello');
        },
      ),
    ),
  );
}

Padding mainText(String text) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Text(
      text,
      style: TextStyle(fontWeight: FontWeight.bold),
    ),
  );
}

Widget qrCodeIcon() {
  return Builder(builder: (context) {
    return IconButton(
      icon: Icon(Icons.qr_code),
      onPressed: () async {
        print("QR oku 2...");
        var result = await scanPlaceQR();
        //isue: qr kodunu placeid= tag'inden sonra al
        if (result != null) {
          showindicator(context, 'Mekana ışınlanıyor...');
          var place = await getCurrentPlace(result);
          closeindicatorOrPreviousPage(context);
          if (place != null) {
            Navigator.push(context, MaterialPageRoute(builder: (context) => PlacePage(place: place)));
          }
        }
      },
    );
  });
}
