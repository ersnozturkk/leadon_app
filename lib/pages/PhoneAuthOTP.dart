import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/firebase/users_table_api.dart';
import 'package:leadon_app/modals/users.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';
import 'package:pinput/pinput.dart';

class PhoneAuthOTP extends StatefulWidget {
  const PhoneAuthOTP({
    Key? key,
    required this.verificationId,
  }) : super(key: key);

  final String verificationId;

  @override
  State<PhoneAuthOTP> createState() => _PhoneAuthOTPState();
}

class _PhoneAuthOTPState extends State<PhoneAuthOTP> {
  @override
  Widget build(BuildContext context) {
    String completedpin = "";
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 56,
      textStyle: TextStyle(fontSize: 20, color: Colors.deepOrangeAccent, fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(color: Color.fromRGBO(234, 239, 243, 1)),
        borderRadius: BorderRadius.circular(20),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
      border: Border.all(color: Color.fromRGBO(114, 178, 238, 1)),
      borderRadius: BorderRadius.circular(8),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration?.copyWith(
        color: Color.fromRGBO(234, 239, 243, 1),
      ),
    );

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.deepOrangeAccent,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Container(
          color: Colors.deepOrangeAccent,
          margin: EdgeInsets.only(left: 25, right: 25),
          alignment: Alignment.center,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Image.asset(
                  'assets/launchericon.png',
                  width: MediaQuery.of(context).size.width - 30,
                  height: (MediaQuery.of(context).size.height / 6),
                ),
                Text(
                  "Telefon Numaranı Doğrula",
                  style: GoogleFonts.nunito(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Giriş için telefon numaranı doğrula",
                  style: GoogleFonts.nunito(fontSize: 16, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 30,
                ),
                Pinput(
                  length: 6,
                  defaultPinTheme: defaultPinTheme,
                  focusedPinTheme: focusedPinTheme,
                  submittedPinTheme: submittedPinTheme,
                  showCursor: true,
                  onCompleted: (pin) async {
                    completedpin = pin;
                    showindicator(context, 'Birazdan bizimlesinnn..');

                    //Kod Gönderildi. Kod girme ekranına git.
                    print("Ersin: CodeSend");
                    String smsCode = completedpin;
                    PhoneAuthCredential credential = PhoneAuthProvider.credential(verificationId: widget.verificationId, smsCode: smsCode);
                    var result = await auth.signInWithCredential(credential);

                    if (result.user != null) {
                      print('Telefonla giriş tamamlandı.');
                      print("Giriş yapan numara: " + result.user!.phoneNumber.toString());
                      print("Giriş yapan id: " + result.user!.uid.toString());
                      print(result.user!.uid);
                      currentUser = await getCurrentUserFromFirebase(result.user!.uid);
                      if (currentUser == null) //ilk giriş ise
                      {
                        print('İlk giriş');
                        currentUser = new LUser(tel: result.user!.phoneNumber, uid: result.user!.uid, usertype: 'user');

                        await SetCurrentUser(currentUser!);
                        closeindicatorOrPreviousPage(context);
                      } else {
                        print('kayıtlı kullanıcı girişi');
                        print(currentUser!.name.toString());
                        currentuserlistenablevalue!.value = currentUser;
                        // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
                        currentuserlistenablevalue!.notifyListeners();
                      }
                      Navigator.popAndPushNamed(context, '/Homepage');
                    } else {}
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: double.infinity,
                  height: 45,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green.shade600, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
                      onPressed: () {
                        print("object");
                      },
                      child: Text("Doğrula ve Giriş Yap")),
                ),
                Row(
                  children: [
                    SizedBox(height: 10),
                    TextButton(
                        onPressed: () {
                          Navigator.pushNamedAndRemoveUntil(context, '/LoginWithPhoneNumber', (route) => false);
                        },
                        child: Text(
                          "Telefon numaranı düzenle",
                          style: TextStyle(color: Colors.white),
                        ))
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//Future<bool> isUseralreadyRegistered(BuildContext context) =>
  
