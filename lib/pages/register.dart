import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/components/snackbar.dart';
import 'package:leadon_app/components/validators.dart';
import 'package:leadon_app/firebase/users_table_api.dart';
import 'package:leadon_app/pages/homepage.dart';
import 'package:leadon_app/modals/input.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController namecontroller = TextEditingController();
  final TextEditingController mailcontroller = TextEditingController();
  final TextEditingController usernamecontroller = TextEditingController();
  final TextEditingController citycontroller = TextEditingController();
  final TextEditingController jobcontroller = TextEditingController();
  final TextEditingController horoscopecontroller = TextEditingController();
  final TextEditingController agecontroller = TextEditingController();

  final mailFormKey = GlobalKey<FormState>();
  final usernameFormKey = GlobalKey<FormState>();
  final nameFormKey = GlobalKey<FormState>();

  XFile? profileimagefile;

  String burcvalue = "";
  ValueNotifier<String>? __burclistenable;

  String citydropdownValue = cities.first;
  String agedropdownValue = ages.first;
  @override
  void initState() {
    __burclistenable = ValueNotifier(burcvalue);
    if (currentUser != null) {
      initTextfields();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: LeadInDefaultAppbar(context),
        backgroundColor: Colors.white,
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 50),
                  registerpageheadtitle(),
                  registerpagesubdescription(),
                  SizedBox(height: 50),
                  GestureDetector(
                      onTap: () async {
                        print("Resim seç");
                        final ImagePicker _picker = ImagePicker();
                        profileimagefile = await _picker.pickImage(source: ImageSource.gallery, imageQuality: 20);
                        setState(() {});
                      },
                      child: Stack(
                        children: [
                          profileimagefile != null ? profileImageFromFile(context) : profileImageFromNetwork(context),
                          Positioned(
                              bottom: 5,
                              right: 10,
                              child: CircleAvatar(
                                backgroundColor: Colors.deepOrange,
                                child: IconButton(
                                  onPressed: () async {
                                    print("Resim seç");
                                    final ImagePicker _picker = ImagePicker();
                                    profileimagefile = await _picker.pickImage(source: ImageSource.gallery, imageQuality: 20);
                                    setState(() {});
                                  },
                                  icon: Icon(
                                    Icons.add,
                                  ),
                                ),
                              )),
                        ],
                      )
                      //child:
                      ),
                  SizedBox(height: 30),
                  namesurnameTextfield(),
                  SizedBox(height: 15),
                  usernameTextfield(),
                  SizedBox(height: 15),
                  mailtextfield(),
                  SizedBox(height: 40),
                  ageandcityCombobox(context),
                  SizedBox(height: 15),
                  jobTextfield(context),
                  SizedBox(height: 15),
                  burcbasliktext(),
                  burclarwidget(),
                  SizedBox(height: 15),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2,
                    height: 45,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.deepOrange,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onPressed: () async {
                          await checkuserinfos(context);
                        },
                        child: Text("Kayıt Ol")),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Future<void> checkuserinfos(BuildContext context) async {
    if (!nameFormKey.currentState!.validate()) {
      showasnackbar(context, 'İsim Soyisim Hatalı', Colors.red, ((p0) {}));
    } else if (!usernameFormKey.currentState!.validate()) {
      showasnackbar(context, 'Kullanıcı adın hatalı', Colors.red, ((p0) {}));
    } else if (!mailFormKey.currentState!.validate()) {
      showasnackbar(context, 'Mail adresin hatalı', Colors.red, ((p0) {}));
    } else if (int.tryParse(agedropdownValue) == null) {
      showasnackbar(context, 'Yaşınızı belirlemediniz.', Colors.red, ((p0) {}));
    } else if (citydropdownValue.contains("Şehir")) {
      showasnackbar(context, 'Sehrinizi seçmediniz.', Colors.red, ((p0) {}));
    } else if (burcvalue.length == 0) {
      showasnackbar(context, 'Burcunuzu seçmediniz.', Colors.red, ((p0) {}));
    } else if (!(profileimagefile != null || currentUser!.imagepath != null)) {
      showasnackbar(context, 'Lütfen bir profil resmi belirleyin.', Colors.red, ((p0) {}));
    } else {
      var file = File(profileimagefile!.path);

      if (profileimagefile != null) currentUser!.imagepath = await UpdateProfileImage(file);
      print('Güncelleniyorrr...');
      currentUser!.name = namecontroller.text;
      currentUser!.username = usernamecontroller.text;
      currentUser!.horoscope = burcvalue;
      currentUser!.age = agedropdownValue;
      currentUser!.mail = mailcontroller.text;
      currentUser!.city = citydropdownValue;
      currentUser!.job = jobcontroller.text;
      // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
      currentuserlistenablevalue!.notifyListeners();

      showindicator(context, 'Bilgileriniz güncelleniyor');
      await SetCurrentUser(currentUser!);
      closeindicatorOrPreviousPage(context);
      closeindicatorOrPreviousPage(context);
    }
  }

  void initTextfields() {
    namecontroller.text = currentUser!.name != null ? currentUser!.name! : "";
    usernamecontroller.text = currentUser!.username != null ? currentUser!.username! : "";
    mailcontroller.text = currentUser!.mail != null ? currentUser!.mail! : "";
    jobcontroller.text = currentUser!.job != null ? currentUser!.job! : "";
    __burclistenable!.value = currentUser!.horoscope != null ? currentUser!.horoscope! : "";
    burcvalue = __burclistenable!.value;
    agedropdownValue = currentUser!.age != null ? currentUser!.age! : ages.first;
    citydropdownValue = currentUser!.city != null ? currentUser!.city! : cities.first;
  }

  CircleAvatar profileImageFromNetwork(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.deepOrange.shade100,
      child: currentUser!.imagepath == null
          ? Icon(Icons.person_add_alt_rounded, size: MediaQuery.of(context).size.width / 4)
          : ClipOval(
              child: CachedNetworkImage(
                imageUrl: currentUser!.imagepath!,
                placeholder: (context, url) => CircularProgressIndicator(strokeWidth: 5),
                errorWidget: (context, url, error) => Icon(Icons.error),
                cacheManager: CacheManager(Config('profileImage', stalePeriod: Duration(days: 2))),
                width: MediaQuery.of(context).size.width / 2,
                height: MediaQuery.of(context).size.width / 2,
                fit: BoxFit.cover,
              ),
            ),
      radius: MediaQuery.of(context).size.width / 4,
    );
  }

  CircleAvatar profileImageFromFile(BuildContext context) {
    return CircleAvatar(
      backgroundImage: Image.file(
        File(profileimagefile!.path),
        fit: BoxFit.cover,
      ).image,
      backgroundColor: Colors.deepOrange.shade100,
      radius: MediaQuery.of(context).size.width / 4,
    );
  }

  Padding burclarwidget() => Padding(padding: const EdgeInsets.symmetric(horizontal: 30), child: Wrap(children: burclar));

  Row burcbasliktext() {
    return Row(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
          child: Text(
            'Burç',
            textAlign: TextAlign.start,
            style: GoogleFonts.nunito(height: 1, fontSize: 18, color: Colors.black54),
          ),
        ),
      ],
    );
  }

  Input2 jobTextfield(BuildContext context) {
    return Input2(
      labeltext: "Meslek",
      controller: jobcontroller,
      onTap: () {
        showSearch(context: context, delegate: JobsSearchDelegate(jobs: jobs, jobtextcontroller: jobcontroller));
      },
    );
  }

  Row ageandcityCombobox(BuildContext context) =>
      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [ageCombobox(context), cityCombobox(context)]);

  Input2 mailtextfield() {
    return Input2(
      formkey: mailFormKey,
      labeltext: "E-Posta",
      controller: mailcontroller,
      validator: (value) {
        if (!mailvalidate.hasMatch(mailcontroller.text)) {
          return 'Geçerli bir mail adresi girin.';
        }
        return null;
      },
    );
  }

  Input2 usernameTextfield() {
    return Input2(
      labeltext: "Kullanıcı Adı",
      formkey: usernameFormKey,
      controller: usernamecontroller,
      validator: (value) {
        if (!usernamevalidate.hasMatch(usernamecontroller.text)) {
          return usernamecontroller.text.length < 8 ? 'Min 8 karakter olmalı...' : 'Kullanıcı adınızda geçersiz özel karakter kullandınız.';
        } else if (usernamecontroller.text.length > 16) {
          return 'Max. 16 karakter olmalıdır.';
        }

        return null;
      },
    );
  }

  Input2 namesurnameTextfield() {
    return Input2(
      labeltext: "İsim Soyisim",
      controller: namecontroller,
      formkey: nameFormKey,
      validator: (value) {
        if (!namevalidator.hasMatch(namecontroller.text) || namecontroller.text.length < 5 || !(namecontroller.text.contains(' '))) {
          return 'Sena Ülkü vb..';
        } else {}
      },
    );
  }

  List<Widget> get burclar {
    return [
      burcItem('Akrep'),
      burcItem('Yengeç'),
      burcItem('Yay'),
      burcItem('Kova'),
      burcItem('Oğlak'),
      burcItem('Terazi'),
      burcItem('Boğa'),
      burcItem('Koç'),
      burcItem('Balık'),
      burcItem('İkizler'),
      burcItem('Aslan'),
      burcItem('Başak')
    ];
  }

  Future<String?> UpdateProfileImage(File imagefile) async {
    var imagesuffix = imagefile.path.split('.').last;
    final firebaseFilePath = FirebaseStorage.instance.ref().child('userImages/' + currentUser!.uid! + '.' + imagesuffix);
    print('İmage Suffix: ' + imagesuffix);
    try {
      showindicator(context, 'Profil resmi güncelleniyor');
      var result_file = await firebaseFilePath.putFile(imagefile);
      String imagepath = await result_file.ref.getDownloadURL();
      closeindicatorOrPreviousPage(context);
      print("image path : " + imagepath);
      return imagepath;
    } catch (e) {
      showasnackbar(context, "Hata: Resim Yukleme" + e.toString(), Colors.red, ((p0) {}));
      print('Hata3 : ' + e.toString());
      return null;
    }
  }

  GestureDetector burcItem(String title) {
    return GestureDetector(
        onTap: () {
          print(title);
          __burclistenable!.value = title;
          burcvalue = __burclistenable!.value;
        },
        child: ValueListenableBuilder(
          valueListenable: __burclistenable!,
          builder: (context, value, child) {
            var bordercolor = title == value ? Colors.deepOrange : Colors.grey;
            double borderwidth = title == value ? 2.5 : 2;
            return Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 10),
              child: Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), border: Border.all(width: borderwidth, color: bordercolor)),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(title),
                ),
              ),
            );
          },
        ));
  }

  Text registerpageheadtitle() => Text('Bilgileri Güncelle',
      style: GoogleFonts.nunito(
        fontWeight: FontWeight.bold,
        fontSize: 24,
        color: Colors.grey.shade900,
      ));

  Padding registerpagesubdescription() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
      child: Text('LeadIn\'ın çok daha eğlenceli içeriklerini görebilmek için üyeliğini tamamla.',
          textAlign: TextAlign.center, style: GoogleFonts.nunito(fontSize: 14, color: Colors.grey.shade700)),
    );
  }

  Container ageCombobox(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 3,
      child: DropdownButton<String>(
        alignment: Alignment.center,
        isExpanded: true,
        value: agedropdownValue,
        icon: Icon(Icons.arrow_downward),
        elevation: 16,
        style: GoogleFonts.nunito(color: Colors.black54, fontSize: 18),
        underline: Container(height: 2, color: Colors.grey),
        onChanged: (String? value) {
          setState(() {
            agedropdownValue = value!;
          });
        },
        items: ages.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Center(
              child: Text(
                value,
                textAlign: TextAlign.center,
              ),
            ),
          );
        }).toList(),
      ),
    );
  }

  Container cityCombobox(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 3,
      child: DropdownButton<String>(
        alignment: Alignment.center,
        isExpanded: true,
        value: citydropdownValue,
        icon: Icon(Icons.arrow_downward),
        elevation: 16,
        style: GoogleFonts.nunito(color: Colors.black54, fontSize: 18),
        underline: Container(height: 2, color: Colors.grey),
        onChanged: (String? value) {
          // This is called when the user selects an item.
          setState(() {
            citydropdownValue = value!;
          });
        },
        items: cities.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Center(
              child: AutoSizeText(
                value.toString() + "  ",
                maxLines: 1,
                textAlign: TextAlign.center,
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}

class JobsSearchDelegate extends SearchDelegate {
  String get searchFieldLabel => 'Mesleğinizi seçin...';
  TextStyle get searchFieldStyle => TextStyle(fontSize: 15);
  JobsSearchDelegate({required this.jobs, required this.jobtextcontroller});
  List<String> jobs;
  TextEditingController jobtextcontroller;
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
          print("Temizle...");
        },
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        query = "";
        close(context, null);
        FocusScope.of(context).unfocus();
        print("Temizle...");
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final List<String> alljobs = jobs.where((element) => element.contains(query)).toList();

    return ListView.builder(
      itemCount: alljobs.length,
      itemBuilder: (context, index) => ListTile(
        title: Text(alljobs.elementAt(index)),
        onTap: () {
          print(alljobs.elementAt(index));
          jobtextcontroller.text = alljobs.elementAt(index);

          close(context, null);
          FocusScope.of(context).unfocus();
        },
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final List<String> alljobs = jobs.where((element) => element.toLowerCase().contains(query.toLowerCase())).toList();

    return ListView.builder(
      itemCount: alljobs.length,
      itemBuilder: (context, index) => ListTile(
        title: Text(alljobs.elementAt(index)),
        onTap: () {
          print(alljobs.elementAt(index));
          jobtextcontroller.text = alljobs.elementAt(index);
          close(context, null);
          FocusScope.of(context).unfocus();
        },
      ),
    );
  }
}

List<String> ages = [
  'Yaş',
  '15',
  '16',
  '17',
  '18',
  '19',
  '20',
  '21',
  '22',
  '23',
  '24',
  '25',
  '26',
  '27',
  '28',
  '29',
  '30',
  '31',
  '32',
  '33',
  '34',
  '35',
  '36',
  '37',
  '38',
  '39',
  '40',
  '41',
  '42',
  '43',
  '44',
  '45',
  '46',
  '47',
  '48',
  '49',
  '50',
  '51',
  '52',
  '53',
  '54',
  '55',
  '56',
  '57',
  '58',
  '59',
  '60',
  '61',
  '62',
  '63',
  '64',
  '65',
  '66',
  '67',
  '68',
  '69'
];

List<String> cities = [
  "Şehir",
  "İSTANBUL",
  "İZMİR",
  "ANKARA",
  "KOCAELİ",
  "BOLU",
  "ADANA",
  "ADIYAMAN",
  "AĞRI",
  "AMASYA",
  "ANTALYA",
  "ARTVİN",
  "AYDIN",
  "AFYONKARAHİSAR",
  "BALIKESİR",
  "BİLECİK",
  "BİNGÖL",
  "BİTLİS",
  "BURDUR",
  "BURSA",
  "ÇANAKKALE",
  "ÇANKIRI",
  "ÇORUM",
  "DENİZLİ",
  "DİYARBAKIR",
  "EDİRNE",
  "ELAZIĞ",
  "ERZİNCAN",
  "ERZURUM",
  "ESKİŞEHİR",
  "GAZİANTEP",
  "GİRESUN",
  "GÜMÜŞHANE",
  "HAKKARİ",
  "HATAY",
  "ISPARTA",
  "MERSİN",
  "KARS",
  "KASTAMONU",
  "KAYSERİ",
  "KIRKLARELİ",
  "KIRŞEHİR",
  "KONYA	",
  "KÜTAHYA",
  "MALATYA",
  "MANİSA",
  "KAHRAMANMARAŞ",
  "MARDİN",
  "MUĞLA",
  "MUŞ",
  "NEVŞEHİR",
  "NİĞDE",
  "ORDU",
  "RİZE",
  "SAKARYA",
  "SAMSUN",
  "SİİRT	",
  "SİNOP",
  "SİVAS",
  "TEKİRDAĞ",
  "TOKAT",
  "TRABZON",
  "TUNCELİ",
  "ŞANLIURFA",
  "UŞAK",
  "VAN",
  "YOZGAT",
  "ZONGULDAK",
  "AKSARAY",
  "BAYBURT",
  "KARAMAN",
  "KIRIKKALE",
  "BATMAN",
  "ŞIRNAK",
  "BARTIN",
  "ARDAHAN",
  "IĞDIR",
  "YALOV",
  "KARABÜK",
  "KİLİS",
  "OSMANİYE",
  "DÜZCE"
];

var jobs = [
  "Acentacı",
  "Acil durum yönetmeni",
  "Acil tıp teknisyeni",
  "Adli tabip",
  "Agronomist",
  "Ağ yöneticisi",
  "Aşçı",
  "Aşçıbaşı",
  "Öğrenci",
  "Ahşap tekne yapımcısı",
  "Aile hekimi",
  "Aktar",
  "Akortçu",
  "Aktör",
  "Aktüer",
  "Aktris",
  "Akustikçi",
  "Albay",
  "Ambalajcı",
  "Ambarcı",
  "Ambulans şoförü",
  "Amiral",
  "Anahtarcı",
  "Analist",
  "Anestezi uzmanı",
  "Anestezi teknikeri",
  "Animatör",
  "Antika satıcısı",
  "Antropolog",
  "Apartman yöneticisi",
  "Araba satıcısı",
  "Araba yıkayıcısı",
  "Arabacı",
  "Arabulucu",
  "Araştırmacı",
  "Arıcı",
  "Arkeolog",
  "Armatör",
  "Arpist",
  "Arşivci",
  "Artist",
  "Asansörcü",
  "Asistan",
  "Asker",
  "Astrofizikçi",
  "Astrolog",
  "Astronom",
  "Astronot",
  "Astsubay",
  "Atlet",
  "Av bekçisi",
  "Avcı",
  "Avizeci",
  "Avukat",
  "Ayakçı (otogar, lokanta)",
  "Ayakkabı boyacısı",
  "Ayakkabı tamircisi",
  "Ayakkabıcı",
  "Ayı oynatıcısı",
  "Apartman görevlisi",
  "Antrenör",
  "Bacacı",
  "Badanacı",
  "Baharatçı",
  "Bahçe bitkileri uzmanı",
  "Bahçıvan",
  "Bakan",
  "Bakıcı",
  "Bakırcı",
  "Bakkal",
  "Bakteriyolog",
  "Balıkçı",
  "Balerin",
  "Balon pilotu",
  "Bankacı",
  "Banker",
  "Barmen",
  "Barmeyd",
  "Basketbolcu",
  "Başbakan",
  "Başçavuş",
  "Başdümenci",
  "Başhemşire",
  "Başkan",
  "Başkomiser",
  "Başpiskopos",
  "Başrahip",
  "Belediye başkanı",
  "Belediye meclisi üyesi",
  "Benzinci",
  "Berber",
  "Besteci",
  "Biletçi",
  "Bilgisayar mühendisi",
  "Bilgisayar programcısı",
  "Bilgisayar tamircisi",
  "Bilim insanı",
  "Bilirkişi",
  "Binicilik",
  "Biracı",
  "Bisikletçi",
  "Biyografi yazarı",
  "Biyolog",
  "Biyomedikal Mühendisi",
  "Bobinajcı",
  "Bombacı",
  "Bomba imhacı",
  "Borsacı",
  "Borucu",
  "Botanikçi",
  "Boyacı",
  "Bozacı",
  "Böcekbilimci",
  "Börekçi",
  "Bulaşıkçı",
  "Buldozer operatörü",
  "Bütçe uzmanı",
  "Büyükelçi",
  "Besicilik",
  "Bebek bakıcısı",
  "Bilgi İşlemci",
  "Camcı",
  "Cerrah",
  "Celep",
  "Cellat",
  "Cost Control",
  "Coğrafyacı",
  "Cillopçu",
  "Cumhurbaşkanı",
  "Çamaşırcı",
  "Çantacı",
  "Çarkçı",
  "Çatıcı",
  "Çaycı",
  "Çevirmen",
  "Çevrebilimci",
  "Çevre mühendisi",
  "Çeyizci",
  "Çıkıkçı",
  "Çıkrıkçı",
  "Çiçekçi",
  "Çiftçi",
  "Çiftlik işletici",
  "Çikolatacı",
  "Çilingir",
  "Çinici",
  "Çitçi",
  "Çoban",
  "Çocuk doktoru",
  "Çorapçı",
  "Çöp işçisi",
  "Çöpçü",
  "Çırak",
  "Çevik Kuvvet",
  "Dadı",
  "Daktilograf",
  "Dalgıç",
  "Damıtıcı",
  "Danışman",
  "Dansöz",
  "Davulcu",
  "Debbağ",
  "Dedektif",
  "Değirmen işçisi",
  "Değirmenci",
  "Demirci",
  "Demiryolu işçisi",
  "Denetçi",
  "Denetleyici",
  "Denizci",
  "Depocu",
  "Derici",
  "Dekorasyoncu",
  "Desinatör",
  "Devlet memuru",
  "Dilci",
  "Dil ve Konuşma Terapisti",
  "Diplomat",
  "Diş hekimi",
  "Diyetisyen",
  "Dizgici",
  "Doğalgazcı",
  "Doğramacı",
  "Doğum uzmanı",
  "Dok işçisi",
  "Dokumacı",
  "Doktor",
  "Dondurmacı",
  "Dökümcü",
  "Döşemeci",
  "Dövizci",
  "Dublajcı",
  "Duvarcı",
  "Dümenci",
  "Diş teknisyeni",
  "Dansçı",
  "Ebe(kadın doğum)",
  "Eczacı",
  "Eczacı kalfası",
  "Editör",
  "Eğitimci",
  "Eğitmen",
  "Ekonomist",
  "Elektrik mühendisi",
  "Elektronik mühendisi",
  "Elektrik-Elektronik mühendisi",
  "Elektronik ve Haberleşme mühendisi",
  "Elektrikçi",
  "Eleştirmen",
  "Embriyolog",
  "Emlakçı",
  "Emniyet amiri",
  "Emniyet genel müdürü",
  "Endüstri mühendisi",
  "Endüstri sistemleri mühendisi",
  "Enstrüman imalatçısı",
  "Ergonomist",
  "Eskici",
  "Esnaf",
  "Estetisyen",
  "Etolojist",
  "Etimolog",
  "Etnolog",
  "Ev hanımı",
  "Fabrika işçisi",
  "Fahişe",
  "Falcı",
  "Fermantasyon işçisi",
  "Fıçıcı",
  "Fırıncı",
  "Figüran",
  "Film yapımcısı",
  "Film yönetmeni",
  "Filozof",
  "Finansör",
  "Fizikçi",
  "Fizyonomist",
  "Fizyoterapist",
  "Fon yöneticisi",
  "Forklift operatörü",
  "Fotoğrafçı",
  "Futbolcu",
  "Gardiyan",
  "Galerici",
  "Garson",
  "Gazete dağıtıcısı",
  "Gazete satıcısı",
  "Gazeteci",
  "Gelir uzmanı",
  "Gelir uzman yardımcısı",
  "Gemici",
  "General",
  "Genetik mühendisi",
  "Geyşa",
  "Gezgin",
  "Gezici vaiz",
  "Gıda mühendisi",
  "Gitarist",
  "Gondolcu",
  "Gökbilimci",
  "Göz doktoru",
  "Gözetmen",
  "Gözlükçü",
  "Grafik Tasarımcısı",
  "Gramer uzmanı",
  "Greyder operatörü",
  "Guru",
  "Güfteci",
  "Gümrük memuru",
  "Gümrük müşaviri",
  "Gümrük müşavir yardımcısı",
  "Gümrük uzmanı",
  "Gündelikçi",
  "Güzellik uzmanı",
  "Gümrük polisi",
  "Haberci",
  "Haddeci",
  "Haham",
  "Hakem",
  "Hakim",
  "Halıcı",
  "Halkbilimci",
  "Hamal",
  "Hamamcı",
  "Hamurkâr",
  "Hareket memuru",
  "Haritacı",
  "Harita mühendisi",
  "Hastabakıcı",
  "Hattat",
  "Hava trafikçisi",
  "Havacı",
  "Haydut",
  "Hayvan bakıcısı",
  "Hayvan terbiyecisi",
  "Hemşire",
  "Hesap uzmanı",
  "Heykeltıraş",
  "Hırdavatçı",
  "Hidrolikçi",
  "Hizmetçi",
  "Hokkabaz",
  "Host",
  "Hostes",
  "Hukukçu",
  "Hurdacı",
  "Hemotolog",
  "Irgat",
  "Işıkçı",
  "İcra memuru",
  "İç mimar",
  "İğneci",
  "İhracatçı",
  "İktisatçı",
  "İlahiyatçı",
  "İllüzyonist",
  "İmam",
  "İnsan kaynakları uzmanı",
  "İnşaat mühendisi",
  "İnşaatçı",
  "İpçi",
  "İplikçi",
  "İstatistikçi",
  "İstihkâmcı",
  "İşaretçi",
  "İşçi",
  "İş güvenliği uzmanı",
  "İşletmeci",
  "İşletme mühendisi",
  "İşportacı",
  "İş ve Uğraşı Terapisti (Ergoterapist)",
  "İş yeri hekimi",
  "İtfaiyeci",
  "İthalatçı",
  "J",
  "Jeofizik mühendisi",
  "Jeoloji mühendisi",
  "Jeolog",
  "Jeomorfolog",
  "Jinekolog",
  "Jimnastikçi",
  "Jokey",
  "K",
  "Kabin görevlisi",
  "Kabuk soyucusu",
  "Kadın berberi",
  "Kadın terzisi",
  "Kâğıtçı",
  "Kahveci",
  "Kâhya",
  "Kalaycı",
  "Kalıpçı",
  "Kaloriferci",
  "Kamarot",
  "Kameraman",
  "Kamyoncu",
  "Kapı satıcısı",
  "Kapıcı",
  "Kaplamacı",
  "Kaportacı",
  "Kaptan",
  "Kardinal",
  "Kardiyolog",
  "Karikatürist",
  "Karoserci",
  "Karpuzcu",
  "Kasap",
  "Kasiyer",
  "Kat görevlisi",
  "Kâtip",
  "Kayıkçı",
  "Kaymakam",
  "Kaynakçı",
  "Kazıcı",
  "Kebapçı",
  "Kemancı",
  "Kesimci",
  "Keskin Nişancı",
  "Kırtasiyeci",
  "Kimyager",
  "Kimya mühendisi",
  "Kitapçı",
  "Klarnetçi",
  "Koleksiyoncu",
  "Komedyen",
  "Komisyoncu",
  "Komiser",
  "Konserveci",
  "Konsolos",
  "Konsomatris",
  "Kontrolör",
  "Konveyör operatörü",
  "Kopyalayıcı",
  "Koreograf",
  "Korgeneral",
  "Koramiral",
  "Korsan",
  "Koruma görevlisi",
  "Komiser",
  "Komiser yardımcısı",
  "Kozmolog",
  "Köfteci",
  "Kömürcü",
  "Köpek eğiticisi",
  "Köşe yazarı",
  "Kuaför",
  "Kuşçu",
  "Kumarbaz",
  "Kumaşçı",
  "Kumcu",
  "Kuru temizlemeci",
  "Kuruyemişçi",
  "Kurye",
  "Kuşbilimci",
  "Kuyumcu",
  "Kürkçü",
  "Kütüphaneci",
  "Krupiye",
  "L",
  "Laborant",
  "Laboratuvar işçisi",
  "Lahmacuncu",
  "Lehimci",
  "Levazımcı",
  "Lobici",
  "Lokantacı",
  "Lokomotifçi",
  "Lostromo",
  "Lostracı",
  "M",
  "Madenci",
  "Makasçı",
  "Makastar",
  "Maketçi",
  "Makinist",
  "Makine mühendisi",
  "Makine zabiti",
  "Makyajcı",
  "Mali hizmetler uzmanı",
  "Manastır baş rahibesi",
  "Manav",
  "Manifaturacı",
  "Manikürcü",
  "Manken",
  "Marangoz",
  "Masör",
  "Masöz",
  "Matador",
  "Matbaacı",
  "Matematikçi",
  "Matkapçı",
  "Medya Planlama Uzmanı",
  "Memur",
  "Menajer",
  "Mermerci",
  "Metalurji mühendisi",
  "Meteoroloji uzmanı",
  "Metin yazarı",
  "Mevsimlik işçi",
  "Meydancı",
  "Meyhaneci",
  "Mezarcı",
  "Midyeci",
  "Mikrobiyolog",
  "Milletvekili",
  "Mimar",
  "Misyoner",
  "Mobilyacı",
  "Modacı",
  "Model",
  "Modelci",
  "Modelist",
  "Montajcı",
  "Montör",
  "Motor tamircisi",
  "Motorcu",
  "Muhabbet tellalı",
  "Muhabir",
  "Muhafız",
  "Muhasebeci",
  "Muhtar",
  "Mumyalayıcı",
  "Muzcu",
  "Mübaşir",
  "Müdür",
  "Müezzin",
  "Müfettiş",
  "Müşavir",
  "Mühendis",
  "Müneccim",
  "Mürebbiye",
  "Müsteşar",
  "Müteahhit",
  "Mütercim",
  "Müze müdürü",
  "Müzik yönetmeni",
  "Müzisyen",
  "Mantarcı",
  "Nalıncı",
  "Nakışçı",
  "Nakliyeci",
  "Nalbant",
  "Nalbur",
  "Obuacı",
  "Ocakçı",
  "Odacı",
  "Oduncu",
  "Odyolog",
  "Okçu",
  "Okul müdürü",
  "Okutman",
  "Operatör",
  "Opera sanatçısı",
  "Orgcu",
  "Orgeneral",
  "Orman mühendisi",
  "Ornitolog (veya kuşbilimci)",
  "Otelci",
  "Oto elektrikçisi",
  "Oto lastik tamircisi",
  "Oto tamircisi",
  "Oto yedek parçacı",
  "Overlokçu",
  "Oymacı",
  "Oyuncu",
  "Oyun hostesi",
  "Oyun yazarı",
  "Oyuncakçı",
  "Öğretmen",
  "Öğretim elemanı",
  "Öğretim görevlisi",
  "Öğretim üyesi",
  "Örmeci",
  "Ön muhasebeci",
  "Ön muhasebe sorumlusu",
  "Ön muhasebe yardımcı elemanı",
  "Ön büro elemanı",
  "Özel şoför",
  "Paketleyici",
  "Palyaço",
  "Pandomimci",
  "Pansiyoncu",
  "Pansumancı",
  "Papa",
  "Papaz",
  "Paralı asker",
  "Park bekçisi",
  "Pastörizör",
  "Patolog",
  "Peçeteci",
  "Pencereci",
  "Perukçu",
  "Peyzaj mimarı",
  "Peyzaj teknikeri",
  "Pideci",
  "Pilavcı",
  "Pilot",
  "Piskopos",
  "Piyade",
  "Piyango satıcısı",
  "Piyanist",
  "Polis memuru",
  "Polis şefi",
  "Polisajcı",
  "Pompacı",
  "Postacı",
  "Profesör",
  "Profil analisti",
  "Proktolog",
  "Protokol görevlisi",
  "Psikiyatr",
  "Psikolog",
  "Psikolojik danışmanlık ve rehberlik",
  "Paramedik",
  "Pornografik film oyuncusu",
  "Radyolog",
  "Redaktör",
  "Rehber",
  "Rejisör",
  "Reklamcı",
  "Rektör",
  "Rektör yardımcısı",
  "Remayözcü",
  "Ressam",
  "Resepsiyon memuru",
  "Rot balansçı",
  "Radyoloji teknisyeni/teknikeri",
  "Saat tamircisi",
  "Saatçi",
  "Sağlık teknisyeni",
  "Sahil koruma",
  "Saksofoncu",
  "Salepçi",
  "Sanat yönetmeni",
  "Sanayici",
  "Sansürcü",
  "Santral memuru",
  "Saraç",
  "Sarraf",
  "Satış elemanı",
  "Savcı",
  "Saz şairi",
  "Sekreter",
  "Senarist",
  "Sepetçi",
  "Serbest muhasebeci mali müşavir",
  "Ses teknisyeni",
  "Seyis",
  "Sınırlı baş makinist",
  "Sicil memuru",
  "Sigortacı",
  "Sihirbaz",
  "Silahçı",
  "Silindir operatörü",
  "Simitçi",
  "Simyacı",
  "Sistem mühendisi",
  "Sistem yöneticisi",
  "Siyasetçi",
  "Soğuk demirci",
  "Sokak çalgıcısı",
  "Sokak satıcısı",
  "Son ütücü",
  "Sorgu hâkimi",
  "Sosyal hizmet uzmanı",
  "Sosyolog",
  "Spiker",
  "Stenograf",
  "Stilist",
  "Striptizci",
  "Su tesisatçısı",
  "Subay",
  "Sucu",
  "Suflör",
  "Sulh hâkimi",
  "Sunucu",
  "Susuz araç yıkama",
  "Sünnetçi",
  "Sürveyan",
  "Sütanne",
  "Sütçü",
  "Şahinci",
  "Şair",
  "Şapel papazı",
  "Şapkacı",
  "Şarap üreticisi",
  "Şarkıcı",
  "Şarkı sözü yazarı",
  "Şarküter",
  "Şekerci",
  "Şemsiyeci",
  "Şifre çözümleyici",
  "Şimşirci",
  "Şoför",
  "Şarapçı",
  "Şarküteri, Gıda Pazarları ve Bakkal Satış Elemanı",
  "Şehir Plancısı",
  "Şıracı",
  "Tabakçı",
  "Tabelacı",
  "Tahsildar",
  "Taksici",
  "Tarım işçisi",
  "Tarihçi",
  "Tasarımcı",
  "Taşçı",
  "Taşlayıcı",
  "Tatlıcı",
  "Tavukçu",
  "Tayfa",
  "Tefeci",
  "Teğmen",
  "Tekniker",
  "Teknisyen",
  "Teknoloji uzmani",
  "Telefon operatörü",
  "Telekız",
  "Televizyon tamircisi",
  "Tellal",
  "Temizlikçi",
  "Temsilci",
  "Terapist",
  "Tercüman",
  "Terzi",
  "Tezgahtar",
  "Tesisatçı",
  "Tesviyeci",
  "Test mühendisi",
  "Test pilotu",
  "Teşrifatçı",
  "Tiyatro yönetmeni",
  "Tombalacı",
  "Topçu",
  "Tornacı",
  "Turizmci",
  "Tuğgeneral",
  "Tuhafiyeci",
  "Turşucu",
  "Tuzcu",
  "Tümamiral",
  "Tümgeneral",
  "Uçuş teknisyeni",
  "Ulaşım sorumlusu",
  "Ustabaşı",
  "Uydu antenci",
  "Uzay mühendisi",
  "Uzay bilimcisi",
  "Uzman Jandarma",
  "Uzman Çavuş",
  "Üretici",
  "Ürolog",
  "Ütücü",
  "Vaiz",
  "Vali",
  "Vantrolog",
  "Vergi denetmeni",
  "Vergi müfettişi",
  "Vergi tahakkuk memuru",
  "Veritabanı yöneticisi",
  "Veri hazırlama ve kontrol işletmeni",
  "Vestiyerci",
  "Veteriner hekim",
  "Veteriner sağlık teknikeri",
  "Veteriner sağlık teknisyeni",
  "Veznedar",
  "Video editörü",
  "Vinç operatörü",
  "Vitrinci",
  "Viyolonselci",
  "Vatman",
  "Vale",
  "Voleybolcu",
  "Yarbay",
  "Yardımcı hakem",
  "Yardımcı hizmetli",
  "Yardımcı pilot",
  "Yargıç",
  "Yatırım uzmanı",
  "Yayıncı",
  "Yazar",
  "Yazı işleri müdürü",
  "Yazılım mühendisi",
  "Yelkenci",
  "Yeminli mali müşavir",
  "Yeminli tercüman",
  "Yer gösterici",
  "Yer teknisyeni",
  "Yerölçmeci",
  "Yoğurtçu",
  "Yol bekçisi",
  "Yorgancı",
  "Yorumcu",
  "Yönetici",
  "Yüzücü",
  "Yönetmen",
  "Zabıta",
  "Zabıt Katibi",
  "Zoolog",
  "Ziraat mühendisi"
];
