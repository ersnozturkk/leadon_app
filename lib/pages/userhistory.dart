import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/buttons.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/firebase/channels_api.dart';
import 'package:leadon_app/firebase/visitorapi.dart';
import 'package:leadon_app/pages/ChannelChat.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';
import 'package:leadon_app/pages/place.dart';

class UserMessagesPage extends StatefulWidget {
  const UserMessagesPage({super.key});

  @override
  State<UserMessagesPage> createState() => _UserMessagesPageState();
}

class _UserMessagesPageState extends State<UserMessagesPage> with TickerProviderStateMixin<UserMessagesPage> {
  TabController? controller;

  @override
  void initState() {
    controller = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: currentuserlistenablevalue!,
      builder: (context, value, child) {
        if (value == null || value.username == null) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // ignore: deprecated_member_use
              Icon(FontAwesomeIcons.sadTear, size: 32, color: Colors.deepOrange),
              SizedBox(height: 10),
              Text(
                'Geçmişi görebilmek için bir kullanıcı oluşturmalısın.',
                textAlign: TextAlign.center,
                style: GoogleFonts.nunito(fontWeight: FontWeight.w300, fontSize: 24),
              ),
              SizedBox(height: 20),
              MaterialButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
                elevation: 16,
                minWidth: 60,
                color: Colors.deepOrange,
                onPressed: () {
                  Navigator.pushNamed(context, '/Register');
                },
                child: Text(
                  "Kullanıcı oluştur",
                  style: GoogleFonts.nunito(),
                ),
              ),
            ],
          );
        }

        return Container(
          color: Colors.white,
          child: Column(
            children: [
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  channelsbutton(),
                  placesbutton(),
                ],
              ),
              userhistorybody(),
            ],
            mainAxisAlignment: MainAxisAlignment.start,
          ),
        );
      },
    );
  }

  Expanded userhistorybody() {
    return Expanded(
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        clipBehavior: Clip.none,
        controller: controller,
        children: [
          placeshistorylist(),
          channelhistorylist(),
        ],
      ),
    );
  }

  Container channelhistorylist() {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: AutoSizeText(
              'Ziyaret Geçmişi - Kanallar',
              maxLines: 1,
              style: GoogleFonts.nunito(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          StreamBuilder(
            stream: getChannelsOfvisitor(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data!.length == 0) {
                  return Center(
                    child: Text('Hiç mekan yok'),
                  );
                }
                var visitedplaces = snapshot.data!;
                return Expanded(
                    child: ListView.builder(
                  itemCount: visitedplaces.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(visitedplaces[index].placename.toString() + " - " + visitedplaces[index].channelname.toString()),
                      subtitle: Text(visitedplaces[index].visittime!.toDate().toString()),
                      leading: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: Icon(
                          Icons.location_on_outlined,
                          size: 32,
                          color: Colors.deepOrange,
                        ),
                      ),
                      onTap: () async {
                        print("channel id : " + visitedplaces[index].channelid!);

                        var currentplace = await getCurrentPlace(visitedplaces[index].placeid!);
                        print("112233: " + currentplace!.placeid.toString());

                        var currentchannel = await getCurrentChannel(currentplace.placeid!, visitedplaces[index].channelid!);
                        print(currentchannel.channelid.toString());
                        setVisitedChanneltoUserTable(currentplace, currentchannel, false);
                        setVisitorToChallelTable(currentplace, currentchannel, false);

                        Navigator.push(context, MaterialPageRoute(builder: (context) => ChannelPage(place: currentplace, channel: currentchannel)));
                      },
                    );
                  },
                ));
              }
              return Center(child: CircularProgressIndicator());
            },
          )
        ],
      ),
    );
  }

  Container placeshistorylist() {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: AutoSizeText(
              'Ziyaret Geçmişi - Mekanlar',
              maxLines: 1,
              style: GoogleFonts.nunito(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          StreamBuilder(
            stream: getPlacesOfvisitor(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data!.length == 0) {
                  return Center(
                    child: Text('Hiç mekan yok'),
                  );
                }
                var visitedplaces = snapshot.data!;
                return Expanded(
                    child: ScrollConfiguration(
                  behavior: ScrollBehavior(),
                  child: GlowingOverscrollIndicator(
                    showLeading: false,
                    showTrailing: false,
                    axisDirection: AxisDirection.down,
                    color: Colors.transparent,
                    child: ListView.builder(
                      itemCount: visitedplaces.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(visitedplaces[index].placename.toString()),
                          subtitle: Text('${visitedplaces[index].visitcount!} ziyaret'),
                          leading: CircleAvatar(
                            backgroundColor: Colors.transparent,
                            child: Icon(
                              Icons.location_on_outlined,
                              size: 32,
                              color: Colors.deepOrange,
                            ),
                          ),
                          onTap: () async {
                            showindicator(context, '..');
                            var currentplace = await getCurrentPlace(visitedplaces[index].placeid!);
                            setVisitedPlaceToUser(currentplace!, false);
                            setVisitorToPlace(currentplace, false);
                            closeindicatorOrPreviousPage(context);

                            Navigator.push(context, MaterialPageRoute(builder: (context) => PlacePage(place: currentplace)));
                          },
                        );
                      },
                    ),
                  ),
                ));
              }
              return Center(child: CircularProgressIndicator());
            },
          )
        ],
      ),
    );
  }

  ElevatedButton channelsbutton() {
    return ElevatedButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.deepOrange),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ))),
      onPressed: () async {
        controller!.animateTo(0);
      },
      child: Text(
        'Mekanlar',
        style: GoogleFonts.nunito(color: Colors.white),
      ),
    );
  }

  ElevatedButton placesbutton() {
    return ElevatedButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.indigoAccent),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ))),
      onPressed: () async {
        controller!.animateTo(1);
      },
      child: Text(
        'Kanallar',
        style: GoogleFonts.nunito(color: Colors.white),
      ),
    );
  }
}
