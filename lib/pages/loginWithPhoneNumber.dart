import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/pages/PhoneAuthOTP.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/modals/input.dart';
import 'package:leadon_app/modals/users.dart';

FirebaseAuth auth = FirebaseAuth.instance;
LUser? currentUser;
ValueNotifier<LUser?>? currentuserlistenablevalue;

class LoginWithPhoneNumber extends StatefulWidget {
  const LoginWithPhoneNumber({Key? key}) : super(key: key);

  @override
  _LoginWithPhoneNumberState createState() => _LoginWithPhoneNumberState();
}

class _LoginWithPhoneNumberState extends State<LoginWithPhoneNumber> {
  final TextEditingController controller2 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Container(
              color: Colors.deepOrangeAccent,
              padding: EdgeInsets.all(30),
              width: double.infinity,
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/launchericon.png',
                    width: MediaQuery.of(context).size.width - 60,
                    height: (MediaQuery.of(context).size.height / 3.4),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    'GİRİŞ YAP',
                    style: GoogleFonts.nunito(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
                    child: Text(
                      'LeadIn\'a ilk kez giriş yapıyorsun.',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.nunito(fontSize: 14, color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                      style: GoogleFonts.nunito(fontSize: 24, color: Colors.white),
                      controller: controller2,
                      onChanged: (value) {
                        if (!value.contains("+90")) {
                          controller2.text = "+90";
                          controller2.selection = TextSelection.fromPosition(TextPosition(offset: 3));
                        }
                      },
                      maxLength: 13,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        labelStyle: GoogleFonts.nunito(fontSize: 14, color: Colors.white),
                        helperStyle: GoogleFonts.nunito(fontSize: 14, color: Colors.white),
                        labelText: "Telefon Numarası",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          borderSide: BorderSide(
                            color: Colors.white,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          borderSide: BorderSide(
                            color: Colors.grey,
                            width: 2.0,
                          ),
                        ),
                      )),
                  SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 45,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onPressed: () async {
                          print("Giriş Yapılıyor.");
                          String fixedPhoneNumber = getFixedPhoneNumber(controller2.text);

                          print("value: $fixedPhoneNumber");
                          showindicator(context, 'Sms gönderiliyor..');

                          await auth.verifyPhoneNumber(
                            phoneNumber: fixedPhoneNumber,
                            verificationCompleted: (PhoneAuthCredential credential) {
                              print("Verification is OK");
                            },
                            verificationFailed: (FirebaseAuthException e) {
                              print("Verification is not OK :  " + e.message.toString());
                            },
                            codeSent: (String verificationId, int? resendToken) async {
                              //Navigator.popAndPushNamed(context, '/phoneCode');
                              Navigator.pop(context);

                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PhoneAuthOTP(verificationId: verificationId),
                                ),
                              );
                            },
                            codeAutoRetrievalTimeout: (String verificationId) {
                              //sms girmediğinde düzenle
                              print("Ersin:  Timeout");
                            },
                          );
                        },
                        child: Text("Giriş Yap")),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Wrap(
                    children: [
                      RichText(
                        text: TextSpan(
                            text: 'Giriş yaparak LeadIn kullanım şartlarını ve gizlilik sözleşmesini kabul etmiş sayılırsınız. ',
                            style: GoogleFonts.nunito(color: Colors.grey.shade700),
                            children: [
                              new TextSpan(
                                text: 'Kullanım şartları ve gizlilik sözleşmesi için tıklayın.',
                                style: GoogleFonts.nunito(
                                    color: Colors.grey.shade700, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.wavy),
                                recognizer: new TapGestureRecognizer()..onTap = () => print('Tap Here onTap'),
                              )
                            ]),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
