import 'dart:async';
import 'package:flutter/material.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/components/snackbar.dart';
import 'package:leadon_app/firebase/users_table_api.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

class InitProvider extends StatelessWidget {
  const InitProvider({super.key});
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    Timer? timer;

    timer = new Timer(new Duration(milliseconds: 100), () async {
      if (auth.currentUser == null) {
        Navigator.popAndPushNamed(context, '/LoginWithPhoneNumber');
      } else {
        await updateCurrentUser();
        Navigator.popAndPushNamed(context, '/Homepage');
      }
      showasnackbar(context, 'Timer Çalıştı.', Colors.amber, ((p0) {}));
      timer!.cancel();
    });
    return Scaffold(
      backgroundColor: Colors.white,
      body: defaultProgressIndicator('Giriş Yapılıyor..'),
    );
  }
}
