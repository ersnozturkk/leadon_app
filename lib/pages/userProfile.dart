import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/modals/settings.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';
import 'package:leadon_app/pages/register.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePageBody extends StatefulWidget {
  const ProfilePageBody({super.key});

  @override
  State<ProfilePageBody> createState() => _ProfilePageBodyState();
}

class _ProfilePageBodyState extends State<ProfilePageBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: ListView(
          children: [
            SizedBox(height: 30),
            ValueListenableBuilder(
              valueListenable: currentuserlistenablevalue!,
              builder: (context, value, child) {
                return currentUser!.imagepath != null ? profileImagefromNetwork(context) : profileImageFromIcon(context);
              },
            ),
            SizedBox(
              height: 5,
            ),
            ValueListenableBuilder(
              valueListenable: currentuserlistenablevalue!,
              builder: (context, value, child) {
                if (currentUser!.username == null)
                  return Column(
                    children: [
                      SizedBox(height: 20),
                      Center(
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
                          elevation: 16,
                          minWidth: 60,
                          color: Colors.deepOrange,
                          onPressed: () {
                            Navigator.pushNamed(context, '/Register');
                          },
                          child: Text(
                            "Profil bilgilerini tamamla",
                            style: GoogleFonts.nunito(),
                          ),
                        ),
                      ),
                    ],
                  );

                return Column(
                  children: [
                    Text(currentUser!.name!,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        )),
                    Text(currentUser!.username!,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          fontStyle: FontStyle.italic,
                        )),
                  ],
                );
              },
            ),
            Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                ListTile(
                    minLeadingWidth: 5,
                    style: ListTileStyle.list,
                    onTap: () {
                      print('Profil Bilgilerini Güncelle');
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Register()));
                    },
                    title: Text('Bilgileri Güncelle', style: GoogleFonts.nunito()),
                    trailing: Icon(Icons.arrow_forward_ios_outlined),
                    leading: Icon(FontAwesomeIcons.pen, size: 20),
                    focusColor: Colors.deepOrange,
                    hoverColor: Colors.deepOrange),
                Divider(height: 0.01),
                ListTile(
                    style: ListTileStyle.list,
                    onTap: () async {
                      var url = Uri.parse(getSettingsValueAsString('privacyurl')!);
                      if (!await launchUrl(url)) {
                        throw 'Could not launch $url';
                      }
                    },
                    minLeadingWidth: 5,
                    title: Text('Gizlilik Sözleşmesi', style: GoogleFonts.nunito()),
                    leading: Icon(FontAwesomeIcons.book),
                    trailing: Icon(Icons.arrow_forward_ios_outlined),
                    focusColor: Colors.deepOrange,
                    hoverColor: Colors.deepOrange),
                Divider(height: 0.01),
                ListTile(
                    style: ListTileStyle.list,
                    onTap: () async {
                      var url = Uri.parse(getSettingsValueAsString('helpurl')!);
                      if (!await launchUrl(url)) {
                        throw 'Could not launch $url';
                      }
                    },
                    minLeadingWidth: 5,
                    title: Text('Yardım ve Destek', style: GoogleFonts.nunito()),
                    leading: Icon(FontAwesomeIcons.question),
                    trailing: Icon(Icons.arrow_forward_ios_outlined),
                    focusColor: Colors.deepOrange,
                    hoverColor: Colors.deepOrange),
                Divider(height: 0.01),
                ListTile(
                    style: ListTileStyle.list,
                    onTap: () async {
                      var url = Uri.parse(getSettingsValueAsString('rateurl')!);
                      if (!await launchUrl(url)) {
                        throw 'Could not launch $url';
                      }
                    },
                    minLeadingWidth: 5,
                    title: Text('Bizi Değerlendir', style: GoogleFonts.nunito()),
                    leading: Icon(FontAwesomeIcons.star),
                    trailing: Icon(Icons.arrow_forward_ios_outlined),
                    focusColor: Colors.deepOrange,
                    hoverColor: Colors.deepOrange),
                Divider(height: 0.01),
                ListTile(
                    style: ListTileStyle.list,
                    onTap: () async {
                      currentUser = null;
                      showindicator(context, 'Çıkış yapılıyor');
                      await auth.signOut();
                      closeindicatorOrPreviousPage(context);
                      Navigator.restorablePopAndPushNamed(context, '/LoginWithPhoneNumber');
                    },
                    minLeadingWidth: 5,
                    title: Text('Çıkış Yap', style: GoogleFonts.nunito()),
                    leading: Icon(FontAwesomeIcons.arrowRightFromBracket),
                    trailing: Icon(Icons.arrow_forward_ios_outlined),
                    focusColor: Colors.deepOrange,
                    hoverColor: Colors.deepOrange),
              ],
            ),
          ],
        ),
      ),
    );
  }

  CircleAvatar profileImagefromNetwork(BuildContext context) {
    return CircleAvatar(
      child: ClipOval(
        child: CachedNetworkImage(
          imageUrl: currentUser!.imagepath!,
          fadeInDuration: Duration(milliseconds: 200),
          fadeOutDuration: Duration(milliseconds: 200),
          placeholder: (context, url) => CircularProgressIndicator(strokeWidth: 5),
          errorWidget: (context, url, error) => Icon(Icons.error),
          cacheManager: CacheManager(Config('profileImage', stalePeriod: Duration(days: 2))),
          width: MediaQuery.of(context).size.width / 2,
          height: MediaQuery.of(context).size.width / 2,
          fit: BoxFit.cover,
        ),
      ),
      backgroundColor: Colors.deepOrange.shade100,
      radius: MediaQuery.of(context).size.width / 4,
    );
  }

  Container profileImageFromIcon(BuildContext context) {
    return Container(
      child: CircleAvatar(
        backgroundColor: Colors.deepOrange.shade100,
        child: Icon(
          Icons.person,
          size: MediaQuery.of(context).size.width / 4,
        ),
        radius: MediaQuery.of(context).size.width / 4,
      ),
    );
  }
}
