import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/Message_bubble.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/firebase/channels_api.dart';
import 'package:leadon_app/firebase/messages_api.dart';
import 'package:leadon_app/modals/ChannelMessages.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/liadInPlaces.dart';
import 'package:leadon_app/modals/settings.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

class ChannelPage extends StatefulWidget {
  const ChannelPage({super.key, @required this.channel, @required this.place});

  final Channel? channel;
  final LeadInPlaces? place;

  State<ChannelPage> createState() => _ChannelPageState();
}

class _ChannelPageState extends State<ChannelPage> with AutomaticKeepAliveClientMixin<ChannelPage> {
  Timer? timerforscrool;
  Timer? timerforupdatevisitor;
  ScrollController? MessagesScrollController = ScrollController();

  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    super.initState();

    timerforupdatevisitor = Timer.periodic(
        Duration(
          milliseconds: getSettingsValueAsNumber('userinfoupdatetimemilisecond') == null ? 60000 : getSettingsValueAsNumber('userinfoupdatetimemilisecond')!,
        ), (timer) {
      setVisitorToChallelTable(widget.place!, widget.channel!, true);
      setVisitedChanneltoUserTable(widget.place!, widget.channel!, true);
      print('Channel setting...');
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Channel currentChannel = widget.channel!;
    LeadInPlaces currentPlace = widget.place!;
    String placedocid = widget.place!.placeid!;
    return Scaffold(
        backgroundColor: Colors.deepOrange.shade400,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            currentChannel.name! + " - " + currentPlace.name!,
            style: GoogleFonts.nunito(fontWeight: FontWeight.bold),
          ),
          leading: IconButton(
              onPressed: () {
                print('kanal timere kapat');
                timerforupdatevisitor!.cancel();
                timerforscrool?.cancel();
                closeindicatorOrPreviousPage(context);
              },
              icon: Icon(Icons.arrow_back)),
          actions: [
            IconButton(
              icon: Icon(
                Icons.info_outlined,
                size: 32,
              ),
              onPressed: () {
                showModalBottomSheet(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  context: context,
                  builder: (context) {
                    return Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(100)),
                        child: ListView(
                          children: [
                            Container(
                              height: 10,
                              width: MediaQuery.of(context).size.width / 5,
                              color: Colors.grey,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Kanal Hakkında',
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        ));
                  },
                );
              },
            )
          ],
        ),
        body: MessagesBody(
          placedocid: placedocid,
          currentChannel: currentChannel,
          showinputfield: true,
          scrollControllerformessages: MessagesScrollController!,
        ));
  }
}

class MessagesBody extends StatefulWidget {
  const MessagesBody({
    Key? key,
    required this.placedocid,
    required this.currentChannel,
    required this.scrollControllerformessages,
    required this.showinputfield,
  }) : super(key: key);

  final String placedocid;
  final Channel currentChannel;
  final bool showinputfield;
  final ScrollController scrollControllerformessages;
  @override
  State<MessagesBody> createState() => _MessagesBodyState();
}

class _MessagesBodyState extends State<MessagesBody> with AutomaticKeepAliveClientMixin<MessagesBody> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      children: [
        StreamBuilder(
          stream: readAllMessages(widget.placedocid, widget.currentChannel.channelid!),
          builder: (context, messages) {
            if (messages.hasData) {
              widget.currentChannel.messages!.clear();
              widget.currentChannel.messages!.insertAll(0, messages.data!);
              var allmessages = widget.currentChannel.messages!.reversed.toList();
              if (allmessages.length == 0)
                return Expanded(
                  flex: 1,
                  child: ListView(
                    controller: widget.scrollControllerformessages,
                    children: [
                      SizedBox(
                        height: 150,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(32.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 24,
                          color: Colors.white,
                          child: Container(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.info_outline,
                                  color: Colors.green,
                                  size: 32,
                                ),
                                Text(
                                  'Bu kanalda son 72 saat içinde hiç konuşma olmadı. Hadi ilk mesajı sen at. Ziyaretçileri buraya topla :)',
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                );

              return Expanded(
                flex: 1,
                child: ListView.builder(
                  cacheExtent: 9999,
                  padding: EdgeInsets.only(top: 150),
                  reverse: true,
                  controller: widget.scrollControllerformessages,
                  itemCount: widget.currentChannel.messages!.length,
                  itemBuilder: (context, index) {
                    print('mesajlar listeleniyor...');
                    //precacheImage(new NetworkImage(allmessages[index].photourl!), context);

                    return BubbleSpecialOne(
                      user: allmessages[index].username!,
                      userphotourl: allmessages[index].photourl!,
                      time: allmessages[index].senttime!,
                      seen: true,
                      tail: true,
                      text: allmessages[index].message!,
                      isSender: allmessages[index].username! == currentUser!.username ? true : false,
                      color: allmessages[index].username! == currentUser!.username ? Color.fromARGB(255, 127, 157, 69) : Color.fromARGB(255, 127, 157, 182),
                      textStyle: GoogleFonts.nunito(
                        fontSize: 17.5,
                        color: Colors.white70,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w300,
                      ),
                    );
                  },
                ),
              );
            }
            return CircularProgressIndicator();
          },
        ),
        widget.showinputfield == true
            ? ChatInputField(
                placedocid: widget.placedocid,
                channeldocid: widget.currentChannel.channelid,
                scrollControllerForMessages: widget.scrollControllerformessages,
              )
            : SizedBox(),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class ChatInputField extends StatefulWidget {
  ChatInputField({super.key, @required this.placedocid, @required this.channeldocid, required this.scrollControllerForMessages});

  final String? placedocid;
  final String? channeldocid;
  final ScrollController scrollControllerForMessages;
  @override
  State<ChatInputField> createState() => _ChatInputFieldState();
}

TextEditingController? textcontrollerformessage;

class _ChatInputFieldState extends State<ChatInputField> {
  @override
  void initState() {
    textcontrollerformessage = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 9,
      ),
      decoration: BoxDecoration(color: Colors.deepOrange.shade400),
      child: Row(
        children: [
          SizedBox(width: 2),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
              decoration: BoxDecoration(
                color: Colors.blueGrey.withOpacity(0.50),
                borderRadius: BorderRadius.circular(16),
              ),
              child: Row(
                children: [
                  Icon(Icons.sentiment_satisfied_alt_outlined),
                  SizedBox(width: 6),
                  Expanded(
                    child: TextField(
                      minLines: 1,
                      maxLines: 4,
                      textInputAction: TextInputAction.send,
                      controller: textcontrollerformessage,
                      decoration: InputDecoration(hintText: 'Mesaj', border: InputBorder.none),
                      onChanged: (value) {
                        //print(textcontrollerformessage!.text);
                      },
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.arrow_forward),
                    onPressed: () {
                      sendMessage(widget.placedocid!, widget.channeldocid!, widget.scrollControllerForMessages);
                    },
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void sendMessage(String placeDocID, String channelDocID, ScrollController scrollControllerForMessages) {
  String messagetext = textcontrollerformessage!.text;
  textcontrollerformessage!.text = '';
  var message = ChannelMessages(
    message: messagetext,
    senttime: Timestamp.fromDate(DateTime.now()),
    username: currentUser!.username,
    photourl: currentUser!.imagepath,
  );

  var docUser = FirebaseFirestore.instance.collection('places').doc(placeDocID).collection('channels').doc(channelDocID).collection('messages').doc();
  if (messagetext.length != 0) {
    docUser.set(message.toJson()).then(
      (value) {
        print("Mesaj Gönderildi.");
        scrollControllerForMessages.animateTo(scrollControllerForMessages.position.minScrollExtent, duration: Duration(milliseconds: 200), curve: Curves.ease);
      },
    );
  }
}
