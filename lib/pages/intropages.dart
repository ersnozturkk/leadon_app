import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/components/location.dart';
import 'package:leadon_app/pages/initProvider.dart';
import 'package:leadon_app/components/getlocation.dart';

class OnBoardingPage extends StatefulWidget {
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  bool isallowLocationPermission = false;
  ValueNotifier<bool>? __isallowLocationPermission;

  @override
  void initState() {
    __isallowLocationPermission = ValueNotifier(isallowLocationPermission);
    checkLocationnPermission().then((value) {
      __isallowLocationPermission!.value = value;
      isallowLocationPermission = value;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var bodyStyle = GoogleFonts.nunito(fontSize: 16, color: Colors.white);

    var pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w700,
        fontFamily: GoogleFonts.nunito().fontFamily!,
        color: Colors.white,
      ),
      bodyTextStyle: bodyStyle,
      bodyPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 32.0),
      pageColor: Colors.deepOrangeAccent,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      isBottomSafeArea: true,
      isProgressTap: true,
      key: introKey,
      globalBackgroundColor: Colors.deepOrangeAccent,
      globalHeader: Align(
        alignment: Alignment.topRight,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 16,
              right: 16,
            ),
            child: Text(""),
          ),
        ),
      ),

      pages: [
        PageViewModel(
          title: "Topluluğun olduğu Heryerde ",
          body: "",
          image: buildImage('assets/1trbg.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Bulunduğun menanı daha eğlenceli hale getirmeye ne dersin?",
          body:
              "Mekana özel sohbet kanalları oluştur, etkileşimini arttır!\n\nArkadaşlarınla yada mekandaki diğer insanlar ile LeadIn oyunlarını oyna\n\n Mekanda neler oluyor kaçırma",
          image: buildImage('assets/22.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Bugün ne yapacağım diye düşünme",
          body: "LeadIn'ın düzenlediği etkinliklere katıl\n\nBirbirinden eğlenceli etkinlikler ile sosyalliğine sosyallik kat",
          image: buildImage(),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "\n\n\n100000'den fazla mekan ve etkinlik",
          body: "",
          image: buildImage(),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "\n\n\nGünlük ortalama 50000 etkileşim",
          body: "",
          image: buildImage(),
          decoration: pageDecoration,
        ),
        PageViewModel(
          useScrollView: false,
          title: "Son Aşama",
          body: "Konuma izin vererek LeadIn'da doyasıya dolaş",
          image: buildImage(),
          footer: ValueListenableBuilder(
            valueListenable: __isallowLocationPermission!,
            builder: (context, value, child) {
              return __isallowLocationPermission!.value == true
                  ? MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      onPressed: () async {
                        showindicator(context, "Konumunuz güncelleniyor...");
                        await getLocation();
                        Navigator.pop(context);
                      },
                      color: Colors.green,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(Icons.location_on),
                          Text(
                            "Konum için izin verildi.",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ))
                  : MaterialButton(
                      color: Colors.deepOrangeAccent,
                      onPressed: () async {
                        showindicator(context, "Konuma izin veriliyor..");

                        await getLocation().then(
                          (value) {
                            print('izin veriyorsun');
                            isallowLocationPermission = true;
                            __isallowLocationPermission!.value = true;
                            print('islocatinalow: ' + __isallowLocationPermission!.value.toString());
                            Navigator.pop(context);
                          },
                        );
                      },
                      child: Text(
                        'Konuma İzin Ver',
                        style: TextStyle(color: Colors.white),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    );
            },
          ),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: false,
      skipOrBackFlex: 0,
      nextFlex: 0,
      showBackButton: true,
      //rtl: true, // Display as right-to-left
      back: const Icon(Icons.arrow_back),
      skip: const Text('Geç', style: TextStyle(fontWeight: FontWeight.w600)),
      next: const Icon(Icons.arrow_forward),
      done: const Text('Bitir', style: TextStyle(fontWeight: FontWeight.w600)),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(16),
      controlsPadding: kIsWeb ? const EdgeInsets.all(12.0) : const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }

  void _onIntroEnd(context) async {
    await Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => InitProvider()),
    );
  }

  Widget buildFullscreenImage() {
    return Image.network(
      'https://ouch-cdn2.icons8.com/n9XQxiCMz0_zpnfg9oldMbtSsG7X6NwZi_kLccbLOKw/rs:fit:392:392/czM6Ly9pY29uczgu/b3VjaC1wcm9kLmFz/c2V0cy9zdmcvNDMv/MGE2N2YwYzMtMjQw/NC00MTFjLWE2MTct/ZDk5MTNiY2IzNGY0/LnN2Zw.png',
      fit: BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
    );
  }

  Widget buildImage([String path = 'assets/88.png', double width = 350]) {
    return Image.asset(
      path,
      fit: BoxFit.cover,
      width: MediaQuery.of(context).size.width - 75,
      height: MediaQuery.of(context).size.width - 75,
    );
  }
}

Future<GeoPoint?> getLocation() async {
  try {
    var position = await determinePosition();
    print(position.latitude.toString() + "," + position.longitude.toString());
    return GeoPoint(position.latitude, position.longitude);
  } catch (e) {}
  return null;
}
