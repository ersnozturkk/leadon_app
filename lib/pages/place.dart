import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/tabbaritems.dart';
import 'package:leadon_app/firebase/channels_api.dart';
import 'package:leadon_app/modals/liadInPlaces.dart';
import 'package:leadon_app/pages/placepage/items.dart';

class PlacePage extends StatefulWidget {
  const PlacePage({Key? key, required this.place}) : super(key: key);

  final LeadInPlaces place;

  @override
  State<PlacePage> createState() => _PlacePageState();
}

class _PlacePageState extends State<PlacePage> with TickerProviderStateMixin {
  ScrollController? scroolcontroller;
  ValueNotifier<int>? __tabbarindexListener;
  TabController? tabcontroller;

  bool _swipeIsInProgress = false;
  bool _tapIsBeingExecuted = false;
  int _selectedIndex = 1;
  int _prevIndex = 1;

  Timer? timer;
  @override
  void initState() {
    super.initState();
    tabcontroller = TabController(length: 5, vsync: this);

    scroolcontroller = ScrollController();
    __tabbarindexListener = ValueNotifier<int>(tabcontroller!.index);

    tabcontroller!.animation?.addListener(tabcontrollerAnimationListener);
    tabcontroller!.addListener(tabcontrollerListener);

    timer = Timer.periodic(Duration(seconds: 60), (timer) async {
      print('mekan bilgileri güncelleme');
      setVisitorToPlace(widget.place, true);
      setVisitedPlaceToUser(widget.place, true);
    });
  }

  @override
  Widget build(BuildContext context) {
    var currentplace = widget.place;
    return WillPopScope(
      onWillPop: () {
        timer!.cancel();
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        floatingActionButton: ValueListenableBuilder(
          valueListenable: __tabbarindexListener!,
          builder: (context, value, child) {
            var floatingButtonKey = PlacesPageTabAndBodies(context, currentplace, scroolcontroller!).keys.toList()[value];
            return placeFloatingActionButton(context, floatingButtonKey, currentplace);
          },
        ),
        body: NestedScrollView(
            controller: scroolcontroller,
            physics: BouncingScrollPhysics(),
            headerSliverBuilder: (context, innerBoxIsScrolled) {
              return [
                SliverOverlapAbsorber(
                  handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverAppBar(
                    leading: IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        timer!.cancel();
                        Navigator.pop(context);
                      },
                    ),
                    expandedHeight: 180,
                    collapsedHeight: 70,
                    stretch: true,
                    snap: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      title: Center(
                          child: AutoSizeText(
                        currentplace.name!,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.asapCondensed(
                          textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                        ),
                      )),
                      stretchModes: [StretchMode.zoomBackground],
                      titlePadding: EdgeInsets.symmetric(vertical: 50),
                      background: currentplace.imageurl == null
                          ? Image.asset('assets/Header.jpg', fit: BoxFit.cover)
                          : Image.network(currentplace.imageurl!, fit: BoxFit.cover),
                    ),
                    bottom: TabBar(
                        unselectedLabelStyle: GoogleFonts.nunito(
                          textStyle: TextStyle(fontWeight: FontWeight.w400, color: Colors.white),
                        ),
                        labelPadding: EdgeInsets.fromLTRB(6, 0, 6, 0),
                        isScrollable: true,
                        indicatorWeight: 0.1,
                        controller: tabcontroller,
                        unselectedLabelColor: Colors.white,
                        labelColor: Colors.black,
                        labelStyle: GoogleFonts.nunito(
                          textStyle: TextStyle(fontWeight: FontWeight.w800),
                        ),
                        tabs: PlacesPageTabAndBodies(context, currentplace, scroolcontroller!).keys.map((e) => tabbarItem(context, e)).toList()),
                  ),
                ),
              ];
            },
            body: TabBarView(
              controller: tabcontroller,
              viewportFraction: 1,
              children: PlacesPageTabAndBodies(context, currentplace, scroolcontroller!).values.toList(),
            )),
      ),
    );
  }

  void tabcontrollerListener() {
    _swipeIsInProgress = false;
    _selectedIndex = tabcontroller!.index;
    __tabbarindexListener!.value = _selectedIndex;

    if (_tapIsBeingExecuted == true) {
      _tapIsBeingExecuted = false;
    } else {
      if (tabcontroller!.indexIsChanging) {
        // this is only true when the tab is changed via tap
        _tapIsBeingExecuted = true;
      }
    }
  }

  void tabcontrollerAnimationListener() {
    if (!_tapIsBeingExecuted && !_swipeIsInProgress && (tabcontroller!.offset >= 0.70 || tabcontroller!.offset <= -0.70)) {
      // detects if a swipe is being executed. limits set to 0.5 and -0.5 to make sure the swipe gesture triggered
      print("swipe  detected");
      int newIndex = tabcontroller!.offset > 0 ? tabcontroller!.index + 1 : tabcontroller!.index - 1;
      _swipeIsInProgress = true;
      _prevIndex = _selectedIndex;
      __tabbarindexListener!.value = newIndex;
    } else {
      if (!_tapIsBeingExecuted &&
          _swipeIsInProgress &&
          ((tabcontroller!.offset < 0.70 && tabcontroller!.offset > 0) || (tabcontroller!.offset > -0.70 && tabcontroller!.offset < 0))) {
        // detects if a swipe is being reversed. the
        print("swipe reverse detected");
        _swipeIsInProgress = false;
        __tabbarindexListener!.value = _prevIndex;
      }
    }
  }
}
