import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/buttons.dart';
import 'package:leadon_app/components/text.dart';
import 'package:leadon_app/firebase/channels_api.dart';
import 'package:leadon_app/firebase/general_api.dart';
import 'package:leadon_app/modals/GoogleAutoComplatePlaces.dart';
import 'package:leadon_app/modals/GoogleNearlyPlaces.dart';
import 'package:leadon_app/modals/input.dart';
import 'package:leadon_app/pages/homepage.dart';

class SearchPlacePage extends StatefulWidget {
  const SearchPlacePage({super.key});

  @override
  State<SearchPlacePage> createState() => _SearchPlacePageState();
}

TextEditingController searchBarTextController = TextEditingController();

class _SearchPlacePageState extends State<SearchPlacePage> with AutomaticKeepAliveClientMixin<SearchPlacePage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Material(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          child: Column(children: [
            SizedBox(
              height: 10,
            ),
            MySearchBar(context, searchBarTextController),
            smallHeadline(context, 'Sana özel kısayol LeadIn Ortak Mekan kanalları'),
            StreamBuilder(
              stream: getleadinPlaceChannel(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Expanded(
                      child: GridView.builder(
                    cacheExtent: 9999,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                opacity: 50,
                                fit: BoxFit.cover,
                                image: NetworkImage('https://www.partisepeti.com/Images/////parti-gozlukleri.jpg'),
                              ),
                            ),
                            child: Center(
                              child: Container(
                                alignment: Alignment.center,
                                height: 40,
                                width: 100,
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(color: Colors.deepOrange, borderRadius: BorderRadius.circular(8)),
                                child: AutoSizeText(
                                  snapshot.data![index].name!,
                                  textAlign: TextAlign.center,
                                  maxLines: 1,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ));
                    },
                  ));
                }
                return CircularProgressIndicator();
              },
            )
          ]),
        ),
      ),
    );
  }
}

class MySearchDelegate extends SearchDelegate {
  String get searchFieldLabel => 'QR kodu okut yada Mekan Ara...';
  TextStyle get searchFieldStyle => TextStyle(fontSize: 15);
  @override
  List<Widget>? buildActions(BuildContext context) {
    print("arama sayfası açık");

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          searchBarTextController.text = '';
          close(context, null);
          FocusScope.of(context).unfocus();
          print("Temizle...");
        },
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return qrCodeIcon();
  }

  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder<Response<String>>(
      future: getNearbylacesFromGooglePlacesAPI(query),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          GoogleNearlyPlace places = googleNearlyPlaceFromJson(snapshot.data!.data!);

          /*if (query.length == 0) {
            return Center(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  mainText('Sana Yakan Gece Mekanları'),
                  mainText('Yemelik İçmelik Mekanlar'),
                ],
              ),
            );
          }*/
          /*if (query.length < 3) {
            return Center(child: CircularProgressIndicator());
          }*/
          return Center(
              child: ListView.builder(
            itemCount: places.results!.length,
            itemBuilder: (context, index) {
              return LocationResultBar(context, query, Colors.orangeAccent, places.results![index]);
            },
          ));
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return FutureBuilder<Response<String>>(
      future: getNearbylacesFromGooglePlacesAPI(query),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          GoogleNearlyPlace places = googleNearlyPlaceFromJson(snapshot.data!.data!);
          if (places.results!.length > 0)
            return Center(
                child: ListView.builder(
              itemCount: places.results!.length,
              itemBuilder: (context, index) {
                return LocationResultBar(context, query, Colors.orangeAccent, places.results![index]);
              },
            ));
          else
            return FutureBuilder<Response<String>>(
              future: getAutoComplatePlacesFromGooglePlacesAPI(query),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  GoogleAutoComplatePlaces places = googleAutoComplatePlacesFromJson(snapshot.data!.data!);
                  return Center(
                    child: ListView.builder(
                      itemCount: places.predictions!.length,
                      itemBuilder: (context, index) {
                        return Column(
                          children: [
                            if (index == 0)
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  'Aradığın mekanı yakınlarda bulamadık. Heryerde arıyoruz...',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.nunito(fontWeight: FontWeight.bold, fontSize: 18),
                                ),
                              ),
                            LocationResultBar(context, query, Colors.orangeAccent.shade100, null, places.predictions![index]),
                          ],
                        );
                      },
                    ),
                  );
                }
                return Center(child: CircularProgressIndicator());
              },
            );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}

Widget MySearchBar(BuildContext context, TextEditingController searchBarTextController) {
  return Input(
    onTap: () {
      showSearch(context: context, delegate: MySearchDelegate());
      print("Arama butonu");
      //currentUser!.currentposition = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      //print(currentUser!.currentposition!.latitude.toString() + " , " + currentUser!.currentposition!.longitude.toString());
    },
    controller: searchBarTextController,
    onChanged: (newValue) {},
    prefixIcon: qrCodeIcon(),
    labeltext: "Mekana git...",
    borderColor: Colors.grey,
    borderwidth: 2,
    suffixIcon: IconButton(
      icon: Icon(Icons.search),
      onPressed: () {
        showSearch(context: context, delegate: MySearchDelegate());
        print("Arama butonu");
      },
    ),
  );
}

String jsonfromResponse(Map<String, dynamic> snapshot) {
  //print("Response : \n");
  JsonEncoder encoder = new JsonEncoder.withIndent(' ');
  String prettyprint = encoder.convert(snapshot);
  print(prettyprint);
  return prettyprint;
}
