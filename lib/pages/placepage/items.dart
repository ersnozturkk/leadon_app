import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/components/indicators.dart';
import 'package:leadon_app/components/snackbar.dart';
import 'package:leadon_app/components/validators.dart';
import 'package:leadon_app/firebase/channels_api.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/input.dart';
import 'package:leadon_app/modals/liadInPlaces.dart';
import 'package:leadon_app/pages/ChannelChat.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

Widget placeFloatingActionButton(BuildContext context, String value, LeadInPlaces currentplace) {
  switch (value) {
    case 'Duvar':
      return FloatingActionButton.extended(
        onPressed: () async {
          if (currentUser!.username == null) {
            showasnackbar(
              context,
              "Duvara girebilmek için giriş yap.",
              Colors.red,
              ((p0) {
                Navigator.pushNamed(
                  context,
                  '/Register',
                );
              }),
            );
          } else {
            var currentchannel = await getCurrentChannel(currentplace.placeid!, null, 1, 'onetime');
            print(currentchannel.name);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ChannelPage(channel: currentchannel, place: currentplace)),
            );
          }
          print(currentUser!.username.toString());
        },
        icon: Icon(Icons.chat),
        label: AutoSizeText(
          'Duvara bir not bırak',
          style: GoogleFonts.nunito(
            textStyle: TextStyle(fontWeight: FontWeight.w600),
          ),
        ),
      );
    case 'Sohbet':
      return FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                scrollable: true,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                title: Text('Sohbet Oluştur'),
                content: Builder(
                  builder: (context) {
                    // Get available height and width of the build area of this widget. Make a choice depending on the size.

                    return CreateChannelDialog(
                      place: currentplace,
                      type: 'Chat?',
                    );
                  },
                ),
              );
            },
          );
        },
        child: Icon(FontAwesomeIcons.message),
        backgroundColor: Colors.deepOrange,
      );

    case 'Bize Ulaş':
      return FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                scrollable: true,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                title: Text('Mekanla bağlantı kur'),
                content: Builder(
                  builder: (context) {
                    return CreateChannelDialog(
                      place: currentplace,
                      type: currentUser!.uid,
                    );
                  },
                ),
              );
            },
          );
        },
        child: Icon(FontAwesomeIcons.message),
        backgroundColor: Colors.deepOrange,
      );

    default:
      return SizedBox();
  }
}

BottomNavigationBarItem BottomNavigationButtonItem(String labeltext, IconData icon) {
  return BottomNavigationBarItem(
    label: labeltext,
    icon: Icon(icon),
  );
}

class CreateChannelDialog extends StatefulWidget {
  CreateChannelDialog({super.key, required this.place, required this.type});

  final LeadInPlaces? place;
  final String? type;
  @override
  State<CreateChannelDialog> createState() => _CreateChannelDialogState();
}

class _CreateChannelDialogState extends State<CreateChannelDialog> {
  final TextEditingController channelnameController = TextEditingController();
  final TextEditingController shortdescriptionController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();

  final channelnameFormKey = GlobalKey<FormState>();
  final shortdescriptionFormKey = GlobalKey<FormState>();
  final descriptionFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    LeadInPlaces place = widget.place!;
    return SingleChildScrollView(
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      child: AnimatedPadding(
        padding: MediaQuery.of(context).viewInsets,
        duration: const Duration(milliseconds: 100),
        child: Column(
          children: [
            Text(place.name.toString(), style: GoogleFonts.nunito(fontWeight: FontWeight.w700)),
            Input2(
              labeltext: "Başlık girin",
              formkey: channelnameFormKey,
              controller: channelnameController,
              validator: (value) {
                if (!channelnamevalidate.hasMatch(channelnameController.text)) {
                  return channelnameController.text.length < 5 ? 'Min 5 karakter olmalı...' : 'Kullanıcı adınızda geçersiz özel karakter kullandınız.';
                } else if (channelnameController.text.length > 20) {
                  return 'Max. 16 karakter olmalıdır.';
                }

                return null;
              },
            ),
            MaterialButton(
              onPressed: () {
                if (!channelnameFormKey.currentState!.validate()) {
                  showasnackbar(context, 'Uygun bir kanal ismi giriniz.', Colors.indigo, ((p0) {}));
                } else {
                  print(channelnameController.text);
                  showModalBottomSheet<void>(
                    // isScrollControlled: true,
                    context: context,
                    builder: (BuildContext context) {
                      return SingleChildScrollView(
                        child: AnimatedPadding(
                          padding: MediaQuery.of(context).viewInsets,
                          duration: const Duration(milliseconds: 100),
                          curve: Curves.decelerate,
                          child: Column(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.symmetric(vertical: 10),
                                  height: 10,
                                  width: MediaQuery.of(context).size.width / 4,
                                  decoration: BoxDecoration(color: Colors.orangeAccent, borderRadius: BorderRadius.circular(8)) // buraya bar ı yap
                                  ),
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Input2(
                                  labeltext: "Kanal hakkında kısa açıklama",
                                  formkey: shortdescriptionFormKey,
                                  controller: shortdescriptionController,
                                  maxlenght: 32,
                                  validator: (value) {
                                    if (!channeldescriptionvalidate.hasMatch(shortdescriptionController.text)) {
                                      return shortdescriptionController.text.length < 5
                                          ? 'Min 5 karakter olmalı...'
                                          : 'Kullanıcı adınızda geçersiz özel karakter kullandınız.';
                                    } else if (shortdescriptionController.text.length > 20) {
                                      return 'Max. 16 karakter olmalıdır.';
                                    }

                                    return null;
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: MaterialButton(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.50)),
                                  onPressed: () {
                                    closeindicatorOrPreviousPage(context);

                                    showModalBottomSheet<void>(
                                      // isScrollControlled: true,
                                      context: context,
                                      builder: (BuildContext context) {
                                        var newchannelname = channelnameController.text;
                                        return SingleChildScrollView(
                                          child: AnimatedPadding(
                                            padding: MediaQuery.of(context).viewInsets,
                                            duration: const Duration(milliseconds: 100),
                                            curve: Curves.decelerate,
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                    margin: EdgeInsets.symmetric(vertical: 10),
                                                    height: 10,
                                                    width: MediaQuery.of(context).size.width / 4,
                                                    decoration:
                                                        BoxDecoration(color: Colors.orangeAccent, borderRadius: BorderRadius.circular(8)) // buraya bar ı yap
                                                    ),
                                                Container(
                                                  padding: EdgeInsets.all(10),
                                                  child: Input2(
                                                    labeltext: "Kanalda neler konuşuluyor?",
                                                    formkey: descriptionFormKey,
                                                    controller: descriptionController,
                                                    maxlenght: 250,
                                                    validator: (value) {
                                                      if (!channeldescriptionvalidate.hasMatch(shortdescriptionController.text)) {
                                                        return descriptionController.text.length < 5
                                                            ? 'Min 5 karakter olmalı...'
                                                            : 'Kullanıcı adınızda geçersiz özel karakter kullandınız.';
                                                      } else if (descriptionController.text.length > 20) {
                                                        return 'Max. 16 karakter olmalıdır.';
                                                      }

                                                      return null;
                                                    },
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.all(10.0),
                                                  child: MaterialButton(
                                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.50)),
                                                    onPressed: () {
                                                      final firebaseUser =
                                                          FirebaseFirestore.instance.collection('places').doc(place.placeid).collection('channels').doc();

                                                      var newchannel = Channel(
                                                        name: newchannelname,
                                                        shortdescription: shortdescriptionController.text,
                                                        description: descriptionController.text,
                                                        type: widget.type,
                                                      );
                                                      firebaseUser.set(newchannel.toJson());
                                                      showasnackbar(context, 'Kanal başarıyla açıldı', Colors.green, ((p0) {}));
                                                      closeindicatorOrPreviousPage(context);
                                                      closeindicatorOrPreviousPage(context);
                                                    },
                                                    color: Colors.orangeAccent,
                                                    child: Row(
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: [
                                                        Text(
                                                          'Bitti',
                                                          style: GoogleFonts.nunito(color: Colors.white),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    ).whenComplete(() => print("Kapanıyor 66 "));
                                  },
                                  color: Colors.orangeAccent,
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        'Sonraki',
                                        style: GoogleFonts.nunito(color: Colors.white),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }
              },
              color: Colors.green,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.50)),
              child: Text(
                'Oluştur',
                style: GoogleFonts.nunito(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
