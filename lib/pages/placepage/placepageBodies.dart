import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/firebase/channels_api.dart';
import 'package:leadon_app/modals/liadInPlaces.dart';
import 'package:leadon_app/pages/ChannelChat.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';
import 'package:leadon_app/pages/placepage/items.dart';
import 'package:timelines/timelines.dart';

class ChannelListPage extends StatelessWidget {
  ChannelListPage({super.key, required this.currentplace, required this.scrollcontroller, required this.channeltype});

  final LeadInPlaces currentplace;
  final ScrollController scrollcontroller;
  final String channeltype;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: readChannels(currentplace.placeid!, channeltype),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data!.length > 0) {
            currentplace.channels = snapshot.data!;
            return Container(
              color: Colors.white,
              child: ListView.builder(
                controller: scrollcontroller,
                itemCount: currentplace.channels.length,
                itemBuilder: (context, index) {
                  return index == 0
                      ? Column(
                          children: [
                            SizedBox(
                              height: 125,
                            ),
                            ListTile(
                              title: Text(currentplace.channels[index].name!),
                              subtitle: Text(currentplace.channels[index].shortdescription.toString()),
                              onTap: () async {
                                print('kanal sayfasına gidiliyor 1');
                                setVisitorToChallelTable(currentplace, currentplace.channels[index], false);
                                setVisitedChanneltoUserTable(currentplace, currentplace.channels[index], false);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => ChannelPage(channel: currentplace.channels[index], place: currentplace)),
                                );
                              },
                            )
                          ],
                        )
                      : ListTile(
                          title: Text(currentplace.channels[index].name!),
                          subtitle: Text(currentplace.channels[index].shortdescription.toString()),
                          onTap: () async {
                            print('kanal sayfasına gidiliyor 1');
                            setVisitorToChallelTable(currentplace, currentplace.channels[index], false);
                            setVisitedChanneltoUserTable(currentplace, currentplace.channels[index], false);
                            Navigator.push(
                                context, MaterialPageRoute(builder: (context) => ChannelPage(channel: currentplace.channels[index], place: currentplace)));
                          },
                        );
                },
              ),
            );
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class ContactPageBody extends StatelessWidget {
  ContactPageBody({super.key, required this.currentplace, required this.scrollcontroller, required this.channeltype});

  final LeadInPlaces currentplace;
  final ScrollController scrollcontroller;
  final String channeltype;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: readChannels(currentplace.placeid!, currentUser!.uid!),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data!.length > 0) {
          currentplace.channels = snapshot.data!;

          return Container(
            color: Colors.white,
            child: ListView.builder(
              controller: scrollcontroller,
              itemCount: currentplace.channels.length,
              itemBuilder: (context, index) {
                return index == 0
                    ? Column(
                        children: [
                          SizedBox(
                            height: 125,
                          ),
                          Text(
                            'Mekan ile son sohbetleriniz\n(Size özeldir)',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.nunito(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          ListTile(
                            leading: CircleAvatar(
                                child: Icon(
                              Icons.question_answer,
                              color: Colors.deepOrange,
                            )),
                            title: Text(currentplace.channels[index].name!),
                            subtitle: Text(currentplace.channels[index].shortdescription.toString()),
                            onTap: () async {
                              print('kanal sayfasına gidiliyor 1');
                              setVisitorToChallelTable(currentplace, currentplace.channels[index], false);
                              setVisitedChanneltoUserTable(currentplace, currentplace.channels[index], false);
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ChannelPage(channel: currentplace.channels[index], place: currentplace)),
                              );
                            },
                          )
                        ],
                      )
                    : ListTile(
                        leading: CircleAvatar(
                            child: Icon(
                          Icons.question_answer,
                          color: Colors.deepOrange,
                        )),
                        title: Text(currentplace.channels[index].name!),
                        subtitle: Text(currentplace.channels[index].shortdescription.toString()),
                        onTap: () async {
                          print('kanal sayfasına gidiliyor 1');
                          setVisitorToChallelTable(currentplace, currentplace.channels[index], false);
                          setVisitedChanneltoUserTable(currentplace, currentplace.channels[index], false);
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => ChannelPage(channel: currentplace.channels[index], place: currentplace)));
                        },
                      );
              },
            ),
          );
        } else {
          return Container(
            color: Colors.yellow,
            child: Column(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 24,
                  color: Colors.white,
                  child: Container(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          // ignore: deprecated_member_use
                          FontAwesomeIcons.smile,
                          color: Colors.green,
                          size: 48,
                        ),
                        SizedBox(height: 15),
                        Text(
                          'İşletme artık çok yakınında. İlk sohbeti başlat. Seni mekanın yetkilileriyle buluşturalıms',
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        MaterialButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.50)),
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  scrollable: true,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                                  title: Text('Mekana bağlan'),
                                  content: Builder(
                                    builder: (context) {
                                      // Get available height and width of the build area of this widget. Make a choice depending on the size.

                                      return CreateChannelDialog(
                                        place: currentplace,
                                        type: currentUser!.uid,
                                      );
                                    },
                                  ),
                                );
                              },
                            );
                          },
                          color: Colors.deepOrange,
                          child: Text(
                            'İlk sohbeti başlat',
                            style: GoogleFonts.nunito(color: Colors.white),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ]),
          );
        }
      },
    );
  }
}

class gamesBody extends StatelessWidget {
  const gamesBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.cyan);
  }
}

class EventsPageBody extends StatelessWidget {
  EventsPageBody({required this.channeltype, required this.currentplace, required this.scrollcontroller});

  final LeadInPlaces currentplace;
  final String channeltype;
  final ScrollController scrollcontroller;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
            stream: readChannels(currentplace.placeid!, channeltype),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var events = snapshot.data!;
                return Timeline.tileBuilder(
                  padding: EdgeInsets.only(top: 150),
                  controller: scrollcontroller,
                  builder: TimelineTileBuilder.fromStyle(
                    contentsAlign: ContentsAlign.alternating,
                    contentsBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: Card(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Text('Timeline Event $index'),
                            Text('Timeline Event $index'),
                            Text('Timeline Event $index'),
                            Text('Timeline Event $index'),
                          ],
                        ),
                      )),
                    ),
                    itemCount: events.length,
                  ),
                );
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            })

        /*Timeline.tileBuilder(
        builder: TimelineTileBuilder.fromStyle(
          contentsAlign: ContentsAlign.alternating,
          contentsBuilder: (context, index) => Padding(
            padding: const EdgeInsets.all(24.0),
            child: Card(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Timeline Event $index'),
            )),
          ),
          itemCount: 40,
        ),
      ),*/
        );
  }
}

class DuvarBody extends StatefulWidget {
  DuvarBody({Key? key, required this.placedocid}) : super(key: key);
  final String placedocid;
  final ScrollController scrollControllerForDuvar = ScrollController();

  @override
  State<DuvarBody> createState() => _DuvarBodyState();
}

class _DuvarBodyState extends State<DuvarBody> with AutomaticKeepAliveClientMixin<DuvarBody> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
      future: getCurrentChannel(widget.placedocid, null, 1, 'onetime'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
            color: Colors.deepOrangeAccent,
            child: MessagesBody(
              placedocid: widget.placedocid,
              currentChannel: snapshot.data!,
              showinputfield: false,
              scrollControllerformessages: widget.scrollControllerForDuvar,
            ),
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class contentBody extends StatelessWidget {
  const contentBody({Key? key, required this.scroolcontroller, required this.currentplace}) : super(key: key);

  final ScrollController scroolcontroller;
  final LeadInPlaces currentplace;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.only(top: 150),
              controller: scroolcontroller,
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Mekan Hakkında:',
                    style: GoogleFonts.nunito(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    currentplace.description!,
                    style: GoogleFonts.nunito(fontWeight: FontWeight.bold, fontSize: 14),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
