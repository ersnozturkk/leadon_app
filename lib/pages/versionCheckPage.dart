import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leadon_app/modals/settings.dart';
import 'package:url_launcher/url_launcher.dart';

class VersionControlPage extends StatelessWidget {
  const VersionControlPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        print('Uygulamayı güncelle1...');
        var url = Uri.parse(getSettingsValueAsString('rateurl')!);
        if (!await launchUrl(url, mode: LaunchMode.inAppWebView)) {
          throw 'Could not launch $url';
        }
      },
      child: Container(
        color: Color(0xFFFF6E40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Card(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: AutoSizeText(
                        'Leadin Yenilendi. \nUygulamayı güncellemeniz gerekiyor...',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.nunito(fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.deepOrangeAccent),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Güncelle',
                            style: GoogleFonts.nunito(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
