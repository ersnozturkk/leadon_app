import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/settings.dart';

/**
 * Place id girilmez ise leadinplaces'e yazar.
 * @param1 : channel (Boş olmasın)
 */
void addNewChannel(Channel newchannel, [String? placeid = ""]) {
  if (placeid == "") placeid = getSettingsValueAsString('leadinplaceid');
  if (placeid!.length > 2)
    FirebaseFirestore.instance.collection('places').doc(placeid).collection('channels').doc().set(newchannel.toJson());
  else
    print('Kanal oluşturulamadı.');
}
