import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:leadon_app/modals/ChannelMessages.dart';

Stream<List<ChannelMessages>> readAllMessages(String placedocid, String channeldocid) {
  debugPrint("Veritabanına baglandı");

  return FirebaseFirestore.instance
      .collection('places')
      .doc(placedocid)
      .collection('channels')
      .doc(channeldocid)
      .collection('messages')
      .orderBy('senttime', descending: true)
      .limit(1000)
      .snapshots()
      .map(
        (snapshot) => snapshot.docs.map((doc) => ChannelMessages.fromJson(doc.data())).toList().reversed.toList(),
      );
}
