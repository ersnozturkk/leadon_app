import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/visitedchannels.dart';
import 'package:leadon_app/modals/visitedplaces.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

Stream<List<Visitedplaces>> getPlacesOfvisitor() {
  var result =
      FirebaseFirestore.instance.collection('users').doc(currentUser!.uid).collection('visitedplaces').orderBy('visittime', descending: true).snapshots();

  return result.map((snapshot) => snapshot.docs.map((e) => Visitedplaces.fromJson(e.data(), e.id)).toList());
}

Stream<List<Visitedchannel>> getChannelsOfvisitor() {
  var result = FirebaseFirestore.instance
      .collection('users')
      .doc(currentUser!.uid)
      .collection('visitedchannel')
      .limit(10)
      .orderBy('visittime', descending: true)
      .snapshots();

  return result.map((snapshot) => snapshot.docs.map((e) => Visitedchannel.fromJson(e.data(), e.id)).toList());
}

Stream<List<Visitedchannel>> getPChannelsOfvisitors() {
  var result = FirebaseFirestore.instance.collection('users').doc(currentUser!.uid).collection('visitedchannel').snapshots();

  return result.map((snapshot) => snapshot.docs.map((e) => Visitedchannel.fromJson(e.data(), e.id)).toList());
}

Future<List<Channel>?> getUserChannels(List<Visitedchannel> userchannels) async {
  List<Channel> channel = [];
  for (var i = 0; i < userchannels.length; i++) {
    var result = await FirebaseFirestore.instance.collection('places').doc(userchannels[i].placeid).collection('channels').doc(userchannels[i].channelid).get();
    channel.add(Channel.fromJson(result.data()!, result.id));
  }
  print("Channel len: " + channel.length.toString());
  return channel; //result.docs.map((e) => LeadInPlaces.fromJson(e.data())).toList();
}
