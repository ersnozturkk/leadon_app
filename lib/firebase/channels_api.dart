import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/liadInPlaces.dart';
import 'package:leadon_app/modals/settings.dart';
import 'package:leadon_app/modals/visitor.dart';
import 'package:leadon_app/modals/visitedchannels.dart';
import 'package:leadon_app/modals/visitedplaces.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

Stream<List<Channel>> readChannels(String placedocid, String channeltype, [int limit = 1000]) {
  return FirebaseFirestore.instance
      .collection('places')
      .doc(placedocid)
      .collection('channels')
      .where('type', isEqualTo: channeltype)
      .limit(limit)
      .snapshots()
      .map((snapshot) => snapshot.docs.map((doc) => Channel.fromJson(doc.data(), doc.id)).toList());
}

Stream<List<Channel>> readEvents(String placedocid, String channeltype, [int limit = 1000]) {
  return FirebaseFirestore.instance
      .collection('places')
      .doc(placedocid)
      .collection('events')
      .limit(limit)
      .snapshots()
      .map((snapshot) => snapshot.docs.map((doc) => Channel.fromJson(doc.data(), doc.id)).toList());
}

/**
 * Bu fonksiyonu read channel stream ı ile birleştir
 */
/*Future<Channel> getCustomChannelsForType(String placedocid, String type, [int limit = 1000]) async {
  var result = await FirebaseFirestore.instance.collection('places').doc(placedocid).collection('channels').where('type', isEqualTo: type).limit(limit).get();

  return Channel.fromJson(result.docs.first.data(), result.docs.first.id);
}*/

Future<Channel> getCurrentChannel(String placedocid, String? channeldocid, [int limit = 1000, String? type = null]) async {
  if (type == null) {
    var result = await FirebaseFirestore.instance.collection('places').doc(placedocid).collection('channels').doc(channeldocid).get();
    return Channel.fromJson(result.data()!, result.id);
  } else {
    var result = await FirebaseFirestore.instance.collection('places').doc(placedocid).collection('channels').where('type', isEqualTo: type).limit(limit).get();
    return Channel.fromJson(result.docs.first.data(), result.docs.first.id);
  }
}

Future<bool> addnewPlaceandDefaultChannels(LeadInPlaces place) async {
  //add chosen place

  var doc = FirebaseFirestore.instance.collection('places').doc(place.placeid); //Doc id yi place id olarak belirle Unique çünkü

  await doc.set(place.toJson());
  //add default channel 1
  doc = FirebaseFirestore.instance.collection('places').doc(place.placeid).collection('channels').doc();
  place.channels.add(Channel(
      name: "Duvar",
      shortdescription: "Mekana tek bir not bırak",
      description: "Mekana tek bir not bırak",
      type: "onetime",
      createdTime: Timestamp.fromDate(
        DateTime.now(),
      )));

  await doc.set(place.channels[0].toJson());

  //add default channel 2
  doc = FirebaseFirestore.instance.collection('places').doc(place.placeid).collection('channels').doc();
  place.channels[0] = Channel(
      name: "Masalar birleşsin.",
      shortdescription: "Sohbet akmıyor mu? O zaman masaları birleştir.",
      description: "Sohbet akmıyor mu? O zaman masaları birleştir.",
      type: "chat",
      createdTime: Timestamp.fromDate(DateTime.now()));
  await doc.set(place.channels[0].toJson());

  //add default channel 3
  doc = FirebaseFirestore.instance.collection('places').doc(place.placeid).collection('channels').doc();
  place.channels[0] = Channel(
    name: "Karma",
    shortdescription: "Yükselenin burda olabilir mi?",
    description: "Yükselenin burda olabilir mi?",
    type: "chat",
    createdTime: Timestamp.fromDate(DateTime.now()),
  );
  await doc.set(place.channels[0].toJson());

  //add default channel 4
  doc = FirebaseFirestore.instance.collection('places').doc(place.placeid).collection('channels').doc();
  place.channels[0] = Channel(
    name: "Haydi Sor!",
    shortdescription: "Kalan kısa açıklaması",
    description: "Uzun bir açıklama olabilir",
    type: "game",
    createdTime: Timestamp.fromDate(DateTime.now()),
  );
  await doc.set(place.channels[0].toJson());

  return true;
}

Stream<List<Channel>> getleadinPlaceChannel() {
  return FirebaseFirestore.instance
      .collection('places')
      .doc(getSettingsValueAsString('leadinplaceid')!)
      .collection('channels')
      .snapshots()
      .map((e) => e.docs.map((doc) => Channel.fromJson(doc.data(), doc.id)).toList());
}

Future<void> setVisitorToPlace(LeadInPlaces currentplace, bool isVisitorInchannel) async {
  var doc = FirebaseFirestore.instance.collection('places').doc(currentplace.placeid).collection('visitors').doc(currentUser!.uid);
  Visitor placevisitor = new Visitor(
    visittime: Timestamp.fromDate(DateTime.now()),
    userid: currentUser!.uid,
    visitcount: 1,
  );

  try {
    if (isVisitorInchannel)
      await doc.update({"visittime": Timestamp.fromDate(DateTime.now())});
    else
      await doc.update({"visitcount": FieldValue.increment(1), "visittime": Timestamp.fromDate(DateTime.now())});
  } catch (e) {
    print("HATA1 : " + e.toString());
    await doc.set(placevisitor.toJson());
  }
}

Future<void> setVisitedPlaceToUser(LeadInPlaces currentplace, bool isVisitorInPlace) async {
  var doc = FirebaseFirestore.instance.collection('users').doc(currentUser!.uid).collection('visitedplaces').doc(currentplace.placeid);
  Visitedplaces placevisitor = new Visitedplaces(
    visittime: Timestamp.fromDate(DateTime.now()),
    visitcount: 1,
    placename: currentplace.name,
  );

  try {
    if (isVisitorInPlace)
      await doc.update({"visittime": Timestamp.fromDate(DateTime.now())});
    else
      await doc.update({"visitcount": FieldValue.increment(1), "visittime": Timestamp.fromDate(DateTime.now())});
  } catch (e) {
    print("HATA2 : " + e.toString());
    await doc.set(placevisitor.toJson());
  }
}

Future<void> setVisitedChanneltoUserTable(LeadInPlaces currentplace, Channel currentChannel, bool isVisitorInPlace) async {
  var doc = FirebaseFirestore.instance.collection('users').doc(currentUser!.uid).collection('visitedchannel').doc(currentChannel.channelid);
  Visitedchannel placevisitor = new Visitedchannel(
    placeid: currentplace.placeid,
    placename: currentplace.name,
    channelname: currentChannel.name,
    visittime: Timestamp.fromDate(DateTime.now()),
    visitcount: 1,
  );

  try {
    if (isVisitorInPlace)
      await doc.update({"visittime": Timestamp.fromDate(DateTime.now())});
    else
      await doc.update({"visitcount": FieldValue.increment(1), "visittime": Timestamp.fromDate(DateTime.now())});
  } catch (e) {
    print("HATA290 : " + e.toString());
    await doc.set(placevisitor.toJson());
  }
}

Future<void> setVisitorToChallelTable(LeadInPlaces currentplace, Channel currentchannel, bool isVisitorInPlace) async {
  var doc = FirebaseFirestore.instance
      .collection('places')
      .doc(currentplace.placeid)
      .collection('channels')
      .doc(currentchannel.channelid)
      .collection('visitors')
      .doc(currentUser!.uid);
  Visitor placevisitor = new Visitor(
    visittime: Timestamp.fromDate(DateTime.now()),
    userid: currentUser!.uid,
    visitcount: 1,
  );

  try {
    if (isVisitorInPlace)
      await doc.update({"visittime": Timestamp.fromDate(DateTime.now())});
    else
      await doc.update({"visitcount": FieldValue.increment(1), "visittime": Timestamp.fromDate(DateTime.now())});
  } catch (e) {
    print("HATA6 : " + e.toString());
    await doc.set(placevisitor.toJson());
  }
}
