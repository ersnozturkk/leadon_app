import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:leadon_app/modals/users.dart';
import 'package:leadon_app/pages/intropages.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

Stream<List<LUser>> readCurrentUser() {
  debugPrint("Veritabanına baglandı");
  return FirebaseFirestore.instance
      .collection('users')
      .limit(1)
      .where('uid', isEqualTo: auth.currentUser!.uid)
      .snapshots()
      .map((snapshot) => snapshot.docs.map((doc) => LUser.fromJson(doc.data(), doc.id)).toList());
}

Future<LUser?> getCurrentUserFromFirebase(String Useruid) async {
  final response = await FirebaseFirestore.instance.collection('users').limit(1).where('uid', isEqualTo: Useruid).get();
  if (response.docs.length > 0) return LUser.fromJson(response.docs.first.data(), response.docs.first.id); // Ersin burayı bi kontrol et
  return null;
}

Future<void> updateCurrentUser() async {
  if (currentUser != null) {
    final userDocid = currentUser!.uid;

    currentUser!.usertoken = await FirebaseMessaging.instance.getToken();
    currentUser!.lastseen = Timestamp.fromDate(DateTime.now());
    currentUser!.location = await getLocation();

    currentuserlistenablevalue!.value = currentUser;
    // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
    currentuserlistenablevalue!.notifyListeners();
    final firebaseUser = FirebaseFirestore.instance.collection('users').doc(userDocid);
    firebaseUser.update({"location": currentUser!.location, "lastseen": Timestamp.fromDate(DateTime.now())}).onError(
        (error, stackTrace) => SetCurrentUser(currentUser!));
  } else {
    if (auth.currentUser != null) {
      LUser newUser = new LUser(uid: auth.currentUser!.uid);
      SetCurrentUser(newUser);
    }
  }
}

Future<void> SetCurrentUser(LUser user) async {
  final userDocid = user.uid;
  print('Kullanıcı ilk kayıt');
  user.usertoken = await FirebaseMessaging.instance.getToken();
  user.tel = auth.currentUser!.phoneNumber;
  user.createdtime = Timestamp.fromDate(DateTime.now());
  user.usertype = 'user';
  final firebaseUser = FirebaseFirestore.instance.collection('users').doc(userDocid);
  currentUser = user;
  return firebaseUser.set(user.toJson());
}
