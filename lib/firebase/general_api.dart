import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:leadon_app/modals/channel.dart';
import 'package:leadon_app/modals/story.dart';
import 'package:leadon_app/pages/loginWithPhoneNumber.dart';

Future<List<Story>> getStories() async {
  final response = await FirebaseFirestore.instance.collection('stories').get();

  if (response.docs.length != 0) return response.docs.map((e) => Story.fromJson(e.data())).toList();
  return [];
}

Future<List<Channel>> getCategoryTemplates(String tag) async {
  final response = await FirebaseFirestore.instance.collection('channelTemplates').where('type', isEqualTo: tag).get();

  if (response.docs.length != 0) return response.docs.map((e) => Channel.fromJson(e.data(), e.id)).toList();
  return [];
}

Future<Response<String>> getNearbylacesFromGooglePlacesAPI(String text) => Dio().get(
      'https://maps.googleapis.com/maps/api/place/nearbysearch/json?keyword=${text}&location=${currentUser!.location!.latitude}+${currentUser!.location!.longitude}&rankby=distance&components=country:tr&key=AIzaSyA7mfFWF3glJFBz991mdESdKR6SBvQLHYQ',
    );

Future<Response<String>> getAutoComplatePlacesFromGooglePlacesAPI(String text) {
  return Dio()
      .get('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${text}&components=country:tr&key=AIzaSyA7mfFWF3glJFBz991mdESdKR6SBvQLHYQ');
}
